// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TallGuysGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class TALLGUYS_API ATallGuysGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
