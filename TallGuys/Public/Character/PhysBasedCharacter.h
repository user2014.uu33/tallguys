#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PhysBasedMovementComponent.h"

#include "PhysBasedCharacter.generated.h"


class UCapsuleComponent;


UCLASS(BlueprintType)
class TALLGUYS_API APhysBasedCharacter : public APawn
{
	GENERATED_BODY()

protected:
		
	virtual void BeginPlay() override;

private:

	UPROPERTY(Category = Camera, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		UCapsuleComponent* CapsuleComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		UPhysBasedMovementComponent* PhysCharMovementComponent1;

public:

	APhysBasedCharacter(const FObjectInitializer& objInitializer = FObjectInitializer::Get());

	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	FORCEINLINE class UPhysBasedMovementComponent* GetCharacterMovement() const { return PhysCharMovementComponent1; }

	//Server movement RPC
	UFUNCTION(unreliable, server)
	void ServerMove(FPhysCharMoveInfo MoveInfo);
	void ServerMove_Implementation(FPhysCharMoveInfo MoveInfo);

	//Client movement RPC
	UFUNCTION(unreliable, NetMulticast)
	void MulticastMove(FPhysCharMoveInfo MoveInfo);
	void MulticastMove_Implementation(FPhysCharMoveInfo MoveInfo);

	UFUNCTION(unreliable, client)
	void ClientServerMoveResponse(FPhysCharMoveInfo MoveInfo);
	void ClientServerMoveResponse_Implementation(FPhysCharMoveInfo MoveInfo);


};
