#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
//#include "Engine/NetSerialization.h"
//#include "Serialization/BitWriter.h"
//#include "Containers/BitArray.h"

#include "PhysBasedMovementReplication.generated.h"

class FSavedMove_PhysBasedChar;
class UPhysBasedMovementComponent;


USTRUCT()
struct FPhysCharMoveInfo
{
	GENERATED_BODY()

	UPROPERTY()
		bool bIsJump;

	UPROPERTY()
		bool bIsLongJump;

	UPROPERTY()
		bool bIsClientMoveValid;

	UPROPERTY()
		FVector OldLocation;

	UPROPERTY()
		FVector NewLocation;

	UPROPERTY()
		FQuat NewRotation;

	UPROPERTY()
		FVector LastForwardInputVector;

	UPROPERTY()
		FVector LastRightInputVector;

	UPROPERTY()
		uint8 MovementMode;

	

	

	//move -> send data on server -> server move char -> compare location with new client location -> 
	// -> if results are close -> make correction on server -> send loc on other clients
	//						   \
	//		if too different	-> decline move on client -> make client to recalc movement
	//
	// !!!!!! in future we should store all input data between move synchro witn server (move stack) !!!!!!


	// !!!!!! make serialisation !!!!!!! 
	// 
	// 
	// 
//		FPhysCharNetworkBits()
//		: SavedPackageMap(nullptr)
//	{
//	}
//
//	bool NetSerialize(FArchive& Ar, UPackageMap* PackageMap, bool& bOutSuccess);
//	UPackageMap* GetPackageMap() const { return SavedPackageMap; }
//
//	//------------------------------------------------------------------------
//	// Data
//
//	// TInlineAllocator used with TBitArray takes the number of 32-bit dwords, but the define is in number of bits, so convert here by dividing by 32.
//	TBitArray<TInlineAllocator<1024 / NumBitsPerDWORD>> DataBits;
//
//private:
//	UPackageMap* SavedPackageMap;
};