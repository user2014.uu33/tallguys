// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "GameFramework/PawnMovementComponent.h"
#include "PhysBasedMovementReplication.h"
#include "Components/CapsuleComponent.h"
#include "WorldCollision.h"

#include "PhysBasedMovementComponent.generated.h"

class APhysBasedCharacter;
class UCapsuleComponent;


//TODO: Serialization
//		Server movement validation


#define SURFACE_Slime EPhysicalSurface::SurfaceType1
#define SURFACE_NonFallSurface EPhysicalSurface::SurfaceType2

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FNeedRespawnDelegate);


UENUM(BlueprintType)
enum class EPhysBasedMovementMode : uint8
{
	WALKING = 0			UMETA(DisplayName = "Walking"),
	JUMPING = 1			UMETA(DisplayName = "Jumping"),
	FALLING = 2			UMETA(DisplayName = "Falling"),
	LONGJUMPING = 3		UMETA(DisplayName = "LongJumping"),
	CLIMBING = 4        UMETA(DisplayName = "Climbing")
};


USTRUCT(BlueprintType)
struct FFloorResult
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FHitResult FloorHitResult;
};

UCLASS( ClassGroup = (Movement), meta = (BlueprintSpawnableComponent))
class TALLGUYS_API UPhysBasedMovementComponent : public UPawnMovementComponent
{
	GENERATED_BODY()
	
private:

	UPROPERTY(replicated)
		EPhysBasedMovementMode			CurrMovementMode;
	

	int NeedServerAdjustmentTickCount = 0;

	bool					bNeedAdjustment = false;

	bool					bIsJump;
	bool					bIsLongJump;

	bool					bIsOnFloor;
	bool					bIsJumpPressed;

	FVector					LastForwardInputVector;
	FVector					LastRightInputVector;

	FVector					OldBaseLocation = FVector::ZeroVector;
	FQuat					OldBaseQuat = FQuat::Identity;

	APhysBasedCharacter*	CharacterOwner;

	FPhysCharMoveInfo		ClientMoveInfo;

	FFloorResult			FloorResult;

	UCapsuleComponent*		UpdatedCapsuleComponent;
	//UCapsuleComponent* CapsuleComp = Cast<UCapsuleComponent>(UpdatedComponent);

	FVector					TargetAdjustmentLocation;
	
	float CurrTimeToStandUp;


	virtual void OnRegister() override;

	virtual void OnCreatePhysicsState() override;

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	//Send move input to server
	void ServerMove();
	
	virtual float GetMaxSpeed() const override { return MaxSpeed; }

	void FindFloor();

	void UpdateBasedMovement(float DeltaTime);

	void UpdateBasedRotation(FRotator& FinalRotation, const FRotator& ReducedRotation);

	bool GetMovementBaseTransform(const UPrimitiveComponent* MovementBase, const FName BoneName, FVector& OutLocation, FQuat& OutQuat);

	bool ValidateClientMove();

	void UpdateMovementMode(float DeltaTime);

	void StartStandingUp();

	void StartPikeMove();

	void ClampPhysVelocity();

	bool IfCanJump();

	bool IfCanLongJump();

	UFUNCTION()
	void OnUpdatedComponentHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);


protected:
	virtual bool ResolvePenetrationImpl(const FVector& Adjustment, const FHitResult& Hit, const FQuat& NewRotation) override;


	void TickClimbingSystem();

	bool IsCanLongJump = true;
public:


	UPROPERTY(BlueprintReadOnly)
		FVector MoveDelta;

	UPROPERTY(BlueprintReadOnly)
		FVector AdjustmentDelta;

	UPROPERTY(BlueprintReadOnly)
		bool bIsLongJumpInProcess;

	/** Time to stand up after fall */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PhysCharMovement")
		float TTSFall;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PhysCharMovement")
		float LongJumpVelocity;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PhysCharMovement")
		float JumpVelocity;

	/** Time to stand up after long jump */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PhysCharMovement")
		float TTSLongJump;

	/** Maximum velocity magnitude allowed for the controlled Pawn. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PhysCharMovement")
		float MaxSpeed;

	/** Setting affecting extra force applied when changing direction, making turns have less drift and become more responsive. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PhysCharMovement", meta = (ClampMin = "0", UIMin = "0"))
		float TurningBoost;

	/** Acceleration applied by input (rate of change of velocity) */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PhysCharMovement")
		float Acceleration;

		/** Deceleration applied when there is no input (rate of change of velocity) */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PhysCharMovement")
		float Deceleration;

	/** Max surface angle to step from 0 to 90 degrees */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PhysCharMovement")
		float MaxSurfaceAngleToStep;

	/** Slide down acceleration if surface angle > MaxSurfaceAngleToStep */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PhysCharMovement")
		float SlideDownAcceleration;

	/** Required impulse to make character fall and lose control */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PhysCharMovement")
		float ImpulseToFall;

	/** Clamp component velocity every tick to this value to avoid flying away */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PhysCharMovement")
		float MaxUpdatedComponentVelocity;

	/** Max sum velocity size, using for clamping final velocity on long jump */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PhysCharMovement")
		float MaxVelocitySize;

	/** Max sum velocity offset, using for increasing long jump upper border velocity size (JongJumpVelocity < MaxVelocitySize + MaxVelocitySizeOffset) */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PhysCharMovement")
		float MaxVelocitySizeOffset;

	/** Required impulse to make character fall and lose control in air */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PhysCharMovement")
		float InAirImpulseToFall;

	/** If character fall due to hit, we add him impuse NormalImpulse*AdditionalImpulseMul to make sure it fall if constrain mode make vel null*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PhysCharMovement")
		float AdditionalImpulseMul;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PhysCharMovement InAir")
		float InAirAccelerationMul;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PhysCharMovement InAir")
		float InAirDecelerationMul;

	/** If character in air we multiply time to stand up on InAirTTSMul (we slow down standing up) */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PhysCharMovement InAir")
		float InAirTTSMul;

	/** Acceleration applied by input on slime */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PhysCharMovement Slime")
		float SlimeAccelerationMul;

	/** Deceleration applied by input on slime */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PhysCharMovement Slime")
		float SlimeDecelerationMul;
	
	/** Slime gravity slide down acceleration */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PhysCharMovement Slime")
		float SlimeSlideDownAcceleration;
	
	UPROPERTY(EditAnywhere, Category = "Climbing")
	int DegreesAngleForStartClimbing;

	UPROPERTY(BlueprintReadWrite)
		bool bNaNAppeared = false;

	UPROPERTY(BlueprintAssignable)
		FNeedRespawnDelegate OnNeedRespawn;

public:

	UPhysBasedMovementComponent(const FObjectInitializer& ObjectInitializer);

	UFUNCTION(BlueprintPure)
		FVector GetInputVector();

	UFUNCTION(BlueprintPure) 
		bool IfOnFloor() { return bIsOnFloor; };

	UFUNCTION(BlueprintPure) 
		FFloorResult GetFloor() { return FloorResult; };

	UFUNCTION(BlueprintPure) 
		EPhysBasedMovementMode GetMovementMode() { return CurrMovementMode; };

	virtual bool IsFalling() const override;

	//Handle move on server
	void HandleServerMove(FPhysCharMoveInfo MoveInfo);
	//Handle move on server and clients
	void HandleMulticastMove(FPhysCharMoveInfo MoveInfo);
	//Move character on server and other clients if they have different position
	void HandleNewClientLocation(FVector NewLocation, FQuat NewRotation);
	//Handle move response from server
	void HandleServerResponse(FPhysCharMoveInfo MoveInfo);

	UFUNCTION(BlueprintCallable)
		void SetForwardInputVector(FVector Input, float Scale);

	UFUNCTION(BlueprintCallable)
		void SetRightInputVector(FVector Input, float Scale);

	UFUNCTION(BlueprintCallable)
		void StartJump();

	UFUNCTION(BlueprintCallable)
		void StartLongJump();

	UFUNCTION(BlueprintCallable)
		void StartClimbing(FVector Newlocation);

	UFUNCTION(BlueprintCallable)
		void FinishClimbing();

	UFUNCTION(BlueprintCallable)
		void SetMovementMode(EPhysBasedMovementMode Mode);

	bool IsJump = true;
protected:
	FHitResult OutHit;

	/** Update Velocity based on input. Also applies gravity. */
	virtual void ApplyControlInputToVelocity(float DeltaTime);

	/** Prevent Pawn from leaving the world bounds (if that restriction is enabled in WorldSettings) */
	virtual bool LimitWorldBounds();

	/** Set to true when a position correction is applied. Used to avoid recalculating velocity when this occurs. */
	UPROPERTY(Transient)
		uint32 bPositionCorrected : 1;
};