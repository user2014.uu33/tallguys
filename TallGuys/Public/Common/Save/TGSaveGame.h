// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"

#include "Common/TGCommonStructures.h"

#include "TGSaveGame.generated.h"

/**
 * 
 */
UCLASS()
class TALLGUYS_API UTGSaveGame : public USaveGame
{
	GENERATED_BODY()
	
public:

	UPROPERTY()
		FPlayerAccountInfo PlayerAccountInfo;

};
