﻿#pragma once

#include "CoreMinimal.h"
#include "Engine/Engine.h"

#include "TGCommonStructures.generated.h"


///
///Auth structures start
///

USTRUCT(BlueprintType)
struct FPlayerAccountInfo
{
	GENERATED_BODY()
public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString AccessToken;//for cloud server

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString RefreshToken;//for cloud server

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString Id;
};

USTRUCT(BlueprintType)
struct FAuthResult
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bIsSuccess = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 ResponseCode;
};

///
///Auth structures finish
///


///
///Items structures start
///

USTRUCT(BlueprintType)
struct FItemVariant
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString Id;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString Name;

	/*UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString AssetPath;*/
};

USTRUCT(BlueprintType)
struct FTGItemInfo
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString Id;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		uint8 Grade;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int CoinsPrice;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int CrystalsPrice;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		uint8 Type;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString CompatCharId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString SeasonId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int ShopCategory;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FItemVariant> Variants;
};

///
///Items structures finish
///

///
///Shop structures start
///


USTRUCT(BlueprintType)
struct FShopOfferInfo
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString Id;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		uint8 Category;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		uint8 Segment;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FTGItemInfo ItemInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 CostCoins;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 CostCrystals;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString Expiratin;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString GivenTime;
};

///
///Shop structures finish
///


///
///Users structures start
///

USTRUCT(BlueprintType)
struct FUserInfo
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString Id;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString Login;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Coins;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Crystals;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Experience;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Level;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool SeasonPassPremium;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<uint8> Roles;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FString> AppliedItems;
};

USTRUCT(BlueprintType)
struct FUserItems
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString UserId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FString> ItemsIds;
};


///
///Users structures finish
///



///
///Real-time server structures start
///

UENUM(BlueprintType)
enum class ETGServerType : uint8
{
	Remote = 0,
	Static = 1,
	Local = 2
};


UENUM(BlueprintType)
enum class EBattleServerState : uint8
{  
	Initializing = 0,
	Ready = 1,
	Waiting = 2,
	Playing = 3,
	Ended = 4
};




USTRUCT(BlueprintType)
struct FServerInfoUsersData
{

	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString id;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString login;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FString> appliedItemsIds;
     
};

USTRUCT(BlueprintType)
struct FServerInfo
{

	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString id;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString battleType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FServerInfoUsersData> usersData;
      
};




//POST​/queue​/v{version}​/BattleQueue INPUT
USTRUCT(BlueprintType)
struct FServerType
{

	GENERATED_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString battleType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 serverType;


};

//GET​/queue​/v{version}​/BattleQueue​/{battleId} OUTPUT
USTRUCT(BlueprintType)
struct FBattleServerInfo
{

	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString id;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 serverPort;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString serverIp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString accessToken;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool ready;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 expectedWaitingTimeSec;


};


//POST​/queue​/v{version}​/BattleQueue OUTPUT
USTRUCT(BlueprintType)
struct FBattleServerId
{

	GENERATED_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString battleId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 expectedWaitingTimeSec;


};




USTRUCT(BlueprintType)
struct FBattleInfo
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString Id;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FUserInfo> UsersInfo;
};


USTRUCT(BlueprintType)
struct FRoundResultInfo
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int RoundNumber;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		uint8 Achievement;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bIsTeam;

};


USTRUCT(BlueprintType)
struct FBattleResultInfo
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString UserId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString BattleId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		uint8 State;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FRoundResultInfo> Rounds;
};



USTRUCT(BlueprintType)
struct FAfterBattleInfo
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		uint8 State;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int OldExp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int NewExp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int OldLvl;
	 
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int NewLvl;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Coins = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Crystalls = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int SeasonCoins = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString SeasonItem;
};

///
///Real-time server structures finish
///


///
/// Season struct start 
/// 


USTRUCT(BlueprintType)
struct FSeasonInfoLevelReward
{

	GENERATED_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString seasonId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 level;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool isPremium;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString itemVariantId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 quantity;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 resourceType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString rewardId;

};

USTRUCT(BlueprintType)
struct FSeasonInfo
{

	GENERATED_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString id;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FSeasonInfoLevelReward> levelRewards;

	FSeasonInfo() {};

};



/// 
/// Season struct finish
/// 

/// 
/// ConfigVersion start
/// 


USTRUCT(BlueprintType)
struct FProjectVersions
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString clientVersion;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString serverVersion;
};


/// 
/// 
/// 