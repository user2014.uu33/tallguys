// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UI/TGHUD.h"
#include "TGStartHUD.generated.h"
/**
 * 
 */

UCLASS()
class TALLGUYS_API ATGStartHUD : public ATGHUD
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "GameHud")
		void ShowConnect();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "GameHud")
		void ShowLobby();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "GameHud")
		void ShowWardrobe();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "GameHud")
		void ShowShop();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "GameHud")
		void ShowSeasonPass();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "GameHud")
		void ShowShopPreview(const FString& OfferId, const FString& Name);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "GameHud")
		void UpdateCoins(int coins);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "GameHud")
		void UpdateShop();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "GameHud")
		void ShowOfferInfo(const FString& OfferId);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "GameHud")
		void ShowAfterBattleInfo();
};
