// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UI/TGHUD.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextRenderComponent.h"
#include "TGInGameHUD.generated.h"

class FTimerManager;
class PlayerState;

/**
 * 
 */
USTRUCT(BlueprintType)
struct FInGameUInfo
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite, Category = "")
	int TotalPlayers;
};

UCLASS()
class TALLGUYS_API ATGInGameHUD : public ATGHUD
{
	GENERATED_BODY()
public:
	ATGInGameHUD();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "GameHud")
	void AddPlayerPassed(APlayerState* PlayerState, int PlayerPassedCount, int MaxPlayers);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "GameHud")
	void ShowEndGame(); // show function

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "GameHud")
	void ShowTimerToStartGame(); // timer before start game

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "GameHud")
	void ShowRemainingTime(); // will set timer after start

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "GameHud")
	void InitUInfo(FInGameUInfo Info);
protected:
	virtual void BeginPlay() override;

};
