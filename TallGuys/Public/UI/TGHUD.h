// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "Blueprint/UserWidget.h"
#include "EdGraph/EdGraphNode.h"
#include "Kismet/GameplayStatics.h"
#include "TGHUD.generated.h"

/**
 * 
 */
UCLASS()
class TALLGUYS_API ATGHUD : public AHUD
{
	GENERATED_BODY()

private:

	UUserWidget* CurrentBlokingWidget; 

public:
	ATGHUD();

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TSubclassOf<class UUserWidget> BlokingWidgetClass;

	UFUNCTION(BlueprintCallable, meta = (DeterminesOutputType = "WidgetClass"))
		UUserWidget* AddWidget(TSubclassOf<class UUserWidget> WidgetClass, bool bShouldCloseCurrentWidget);

	UFUNCTION(BlueprintCallable, Category = "Optimization", meta = (DefaultToSelf = Object))
		void RemoveCurrentWidget();

	UFUNCTION(BlueprintCallable, Category = "Optimization")
		UUserWidget* GetCurrentWidget();

	UFUNCTION(BlueprintCallable)
		void SetBlokingWidget(bool bValue);
	

protected:

	UPROPERTY(BlueprintReadOnly)
	UUserWidget* CurrentWidget;
};
