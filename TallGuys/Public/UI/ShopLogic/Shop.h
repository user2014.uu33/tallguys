// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Shop.generated.h"

UCLASS()
class TALLGUYS_API AShop : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AShop();

protected:

	virtual void BeginPlay() override;
private:

public:	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "ShopItemsSettings")
	int TotalElementsForName;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "ShopItemsSettings")
	int TotalElementsForPrice;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "ShopItemsSettings")
	FString Name;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "ShopItemsSettings")
	float Price;

	FString** map;
	
	virtual void Tick(float DeltaTime) override;

	//return "n" items for shop
	UFUNCTION()
	void Genrate();

	void Init();
};
