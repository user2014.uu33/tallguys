// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "Net/UnrealNetwork.h"
#include "BoxTile.generated.h"

class UMaterialInstanceDynamic;

UCLASS()
class TALLGUYS_API ABoxTile : public AActor
{
	GENERATED_BODY()
public:
	// Sets default values for this actor's properties
	ABoxTile();
protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UStaticMeshComponent* MeshComp;

	UPROPERTY(VisibleAnywhere, Category = "Compomemts")
	UBoxComponent* OverlapComp;
	UFUNCTION()
	void OverlapTile(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Behaviour")
	void OnActivate();

	void SetStatic(bool flag);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Behaviour")
	bool bIsStatic;
};