﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Gameplay/Environment/CircleRandomTiles/CircleTile.h"
#include "GameFramework/Actor.h"
#include "Kismet/GameplayStatics.h"
#include "RandCircleFallTiles.generated.h"

UCLASS()
class TALLGUYS_API ARandCircleFallTiles : public AActor
{
	GENERATED_BODY()

public:	
	ARandCircleFallTiles();
protected:
	virtual void BeginPlay() override;

public:	
	virtual void OnConstruction(const FTransform& Transform) override;

	void PostInitializeComponents() override;
	
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditDefaultsOnly, Category = "Behaviour")
	int TileCount = 8;	

	UPROPERTY(EditDefaultsOnly, Category = "Behaviour")
	TSubclassOf<ACircleTile> TileClass;

	UPROPERTY(EditDefaultsOnly, Category = "OnConstruction")
	bool IsOnConstruct = false;


	UPROPERTY(EditAnywhere, Category = "Behaviour")
	float FirstDelay;

	UPROPERTY(EditAnywhere, Category = "Behaviour")
	float NextDelay;

	ACircleTile* CircleTile;
	TArray<AActor*> FoundTilesInWorld;
	TArray<int> CalcRemoveTiles;
	TArray<ACircleTile*> RemovingTilesDuringTheGame;
	
	void InitArrayForRemoveTiles();
	void RemovePreConstructTiles();

	UFUNCTION()
	void GenerateSpawn();                                    
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Start Timer")
	void StartTimerForWaitingPlayers();

	//state
	UPROPERTY(EditAnywhere, Category = "Behaviour")
	FVector Location;
	UPROPERTY(EditAnywhere, Category = "Behaviour")
	FRotator Rotation;
	UPROPERTY(EditAnywhere, Category = "Behaviour")
	float Angle;

	bool IfHasSpawn = false;
private:
	//behavior Tiles
	void RandomFall();

	//Time manegment 
	void SetTimer();
	void DelayBeforeFall();

	//Base algorithm
	void GenerateRandomIndexForTile();

	//maybe will be remove
	int Step;
};
