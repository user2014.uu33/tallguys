// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Net/UnrealNetwork.h"
#include "CircleTile.generated.h"

UCLASS()
class TALLGUYS_API ACircleTile : public AActor
{
	GENERATED_BODY()
	
public:	
	ACircleTile();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditDefaultsOnly, Replicated)
	UParticleSystem* ActiveTileEffect;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Visual Effect")
	void EffectStart();

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const;
};
