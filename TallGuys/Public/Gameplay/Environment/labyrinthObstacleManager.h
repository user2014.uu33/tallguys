#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BoxTile.h"

#include "Components/ChildActorComponent.h"
#include "Components/SphereComponent.h"
#include "labyrinthObstacleManager.generated.h"

class USphereComponent;

UCLASS()
class TALLGUYS_API AlabyrinthObstacleManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AlabyrinthObstacleManager(const FObjectInitializer& ObjectInitializer);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
public:	
	virtual void Tick(float DeltaTime) override;

	int GetRandomNumber(int min, int max);

	UFUNCTION()
	void GenerateLabyrinth();

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Behaviour")
		TArray<ABoxTile*> AllTiles;

	UPROPERTY(EditAnywhere, Category = "Behaviour")
	FRotator Rot;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Behaviour")
	float DistanceX;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Behaviour")
	float DistanceY;

	float DistanceZ;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Behaviour")
	int32 SizeX;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Behaviour")
	int32 SizeY;

	bool t = true; 

	//UPROPERTY(EditAnywhere)
	int** Map;

	//UPROPERTY(EditAnywhere)
	//int Maps[5];

	UPROPERTY(EditDefaultsOnly, Category = "Spawning")
	TSubclassOf<ABoxTile> TileClass;

	FActorSpawnParameters SpawnParams;

	UFUNCTION()
	void SpawnLabyrinth();
	
	void Init();
};
