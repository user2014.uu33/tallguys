
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GuessingFieldTile.h"
#include "GuessingField.generated.h"

UCLASS()
class TALLGUYS_API AGuessingField : public AActor
{
	GENERATED_BODY()
	
public:	
	AGuessingField();

	void Init();

	UFUNCTION()
	void Logic();
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, Category = "Behaviour")
	FRotator Rot;

	int** Map;
	FActorSpawnParameters SpawnParams;

	UFUNCTION()
	void SpawnLabyrinth();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Behaviour")
	float DistanceX;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Behaviour")
	float DistanceY;

	float DistanceZ;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Behaviour")
	int32 SizeX;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Behaviour")
	int32 SizeY;

	UPROPERTY(EditDefaultsOnly, Category = "Spawning")
	TSubclassOf<AGuessingFieldTile> TileClass;

	//Colors spawn
	int32_t red, blue, green;
	FTimerHandle TimerHandle;
};
