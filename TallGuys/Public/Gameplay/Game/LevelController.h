// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Gameplay/Game/NextLevelRule.h"
#include "Engine/DataTable.h"

#include "LevelInfo.h"

#include "LevelController.generated.h"

UCLASS(Blueprintable)
class TALLGUYS_API ULevelController : public UObject
{
	GENERATED_BODY() 
public:
	ULevelController(const FObjectInitializer& objInitializer);

	virtual void PostInitProperties() override;

	UFUNCTION(BlueprintCallable)
	void StartNextLevel(int32 PlayerCount);

	UFUNCTION(BlueprintCallable)
	void SetNextLvLRuleClass(UNextLevelRule* NextLvLRule);

	FLevelData LevelRules;

	UPROPERTY(EditAnywhere)
	TArray<FString> LevelSequence;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UNextLevelRule* NextLevelRule;

	UPROPERTY(EditAnywhere)
	TSubclassOf<UNextLevelRule> NextLevelRuleClass;

	FString contextString;
	FString CurLvl;
	TArray<FLevelInfo*> RowNames;

};
