// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gameplay/Game/NextLevelRule.h"
#include "Gameplay/Game/LevelController.h"
#include "TestRules.generated.h"

UCLASS()
class TALLGUYS_API UTestRules : public UNextLevelRule
{
	GENERATED_BODY()
public:
	FString CurrentLevel;
	int It = 0;
	virtual FString GetNextLevel_Implementation(const FLevelData& LevelRules, const TArray<FLevelInfo>& RowNames) override;
	void SetLevels();
};
