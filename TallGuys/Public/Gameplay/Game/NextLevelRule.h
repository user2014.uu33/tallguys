// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Engine/World.h"
#include "LevelInfo.h"
#include "NextLevelRule.generated.h"

USTRUCT(BlueprintType)
struct FLevelData
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 PlayerNumber;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 LevelNum;
};

UCLASS(Blueprintable)
class TALLGUYS_API UNextLevelRule : public UObject
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintNativeEvent) 
	FString GetNextLevel(const FLevelData& LevelRules, const TArray<FLevelInfo>& RowNames);

	virtual FString GetNextLevel_Implementation(const FLevelData& LevelRules, const TArray<FLevelInfo>& RowNames);
};
