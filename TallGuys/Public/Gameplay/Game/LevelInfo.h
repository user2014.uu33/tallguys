#pragma once


#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Engine/DataTable.h"
//#include "LevelSequence.h"
#include "Runtime/LevelSequence/Public/LevelSequence.h"
#include "LevelInfo.generated.h"

//class ULevelSequence;

UENUM(BlueprintType)

enum TypeMap
{
	Race = 0		UMETA(DisplayName = "Race"),
	Survival = 1	UMETA(DisplayName = "Survival"),
	Team = 2		UMETA(DisplayName = "Team"),
	Final = 3		UMETA(DisplayName = "Final"),
	Hunt = 4		UMETA(DisplayName = "Hunt")
};

USTRUCT(BlueprintType)
struct FLevelInfo : public FTableRowBase
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 MinPlayers;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 MaxPlayers;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString LvLName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ULevelSequence* LevelSequence;
};