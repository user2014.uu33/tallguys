// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "VaRestSubsystem.h"
#include "TGVaRestSubsystem.generated.h"

/**
 * 
 */


USTRUCT(BlueprintType)
struct FVaRestHeader
{
	GENERATED_BODY()
public:
	UPROPERTY()
		FString HeaderName;
	UPROPERTY()
		FString HeaderValue;
};


UCLASS()
class TALLGUYS_API UTGVaRestSubsystem : public UVaRestSubsystem
{
	GENERATED_BODY()

public:
		UFUNCTION(BlueprintCallable, Category = "VaRest|Utility")
		void CallURLCustomHeaders(const FString& URL, EVaRestRequestVerb Verb, EVaRestRequestContentType ContentType, UVaRestJsonObject* VaRestJson, const FVaRestCallDelegate& Callback, TArray<FVaRestHeader> Headers);

};
