#pragma once

#include "CoreMinimal.h"

#include "Json.h"
#include "Misc/Base64.h"

#include "JWTObject.generated.h"


USTRUCT()
struct FJWTObject
{
	GENERATED_BODY()
		
private:
	FString Token;
	TSharedPtr<FJsonObject> JWTHeader;
	TSharedPtr<FJsonObject> JWTPayLoad;

public:
	bool SetToken(FString InToken)
	{
		bool bIsSuccess = true;
		UE_LOG(LogTemp, Display, TEXT("--------- Set %s"), *InToken);

		if (!InToken.IsEmpty())
		{

			Token = InToken;

			TArray<FString> DividedJWT;
			if (InToken.ParseIntoArray(DividedJWT, TEXT(".")) > 0)
			{

				FString DecodedJWTHeader;
				FString DecodedJWTPayLoad;
				bIsSuccess = FBase64::Decode(DividedJWT[0], DecodedJWTHeader) ? bIsSuccess : false;
				bIsSuccess = FBase64::Decode(DividedJWT[1], DecodedJWTPayLoad) ? bIsSuccess : false;



				TSharedPtr<TJsonReader<TCHAR>> JsonReader;

				JsonReader = TJsonReaderFactory<TJsonReader<TCHAR>>::Create(DecodedJWTHeader);
				bIsSuccess = FJsonSerializer::Deserialize(*JsonReader, JWTHeader) ? bIsSuccess : false;


				JsonReader = TJsonReaderFactory<TJsonReader<TCHAR>>::Create(DecodedJWTPayLoad);
				bIsSuccess = FJsonSerializer::Deserialize(*JsonReader, JWTPayLoad) ? bIsSuccess : false;



				JsonReader.Reset();
			}
		}
		else
		{

			bIsSuccess = false;
		}

		UE_LOG(LogTemp, Display, TEXT("--------- Token set %s"), bIsSuccess? L"successfully": L"unsuccessfully!");

		return bIsSuccess;
	}

	FString GetToken()
	{
		return Token;
	}

	TSharedPtr<FJsonObject> GetTokenHeaderJson()
	{
		return JWTHeader;
	}

	TSharedPtr<FJsonObject> GetTokenPayloadJson()
	{
		return JWTPayLoad;
	}
};