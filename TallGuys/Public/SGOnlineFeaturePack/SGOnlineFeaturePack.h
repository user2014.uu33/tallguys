// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"

#include "Engine/Engine.h"
#include "Net/UnrealNetwork.h"
#include "Online.h"
#include "JsonObjectConverter.h"

#include "JsonWebToken/JWTObject.h"
#include "RequestStamps/RequestStamps.h"

#include "VaRestHeaders/TGVaRestSubsystem.h"
//#include "SGHttpCommunication.h"


#include "SGOnlineFeaturePack.generated.h"


DECLARE_LOG_CATEGORY_EXTERN(SGLogSGOnlineFeaturePack, Log, All);

USTRUCT(BlueprintType)
struct FTGVersion
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString ProjectVersion;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString BuildVersion;
};


UENUM(BlueprintType)
enum class ESubsystemType : uint8
{
	FACEBOOK = 0 UMETA(DisplayName = "Facebook"),
	GOOGLE = 1 UMETA(DisplayName = "Google")
};

UENUM(BlueprintType)
enum class ESGOnlineFeatureMessageType : uint8
{
	MESSAGE UMETA(DisplayName = "Message"),
	CRITICAL_ERROR UMETA(DisplayName = "CriticalError")
};

UENUM(BlueprintType)
enum class ESGOnlineFeatureErrorType : uint8
{
	NO_ERROR UMETA(DisplayName = "NoError"),
	INVALID_REFRESH_TOKEN UMETA(DisplayName = "InvalidRefreshToken"),
	FAILED_TO_REFRESH_TOKEN UMETA(DisplayName = "FailedToRefreshToken")
};

USTRUCT(BlueprintType)
struct FSGOnlineFeatureSubsystemMessage
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		ESGOnlineFeatureMessageType MessageType;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		ESGOnlineFeatureErrorType ErrorType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString Description;
};


USTRUCT()
struct FFrozeRequest
{
	GENERATED_BODY()

	FSGRequestStamp Stamp;
	FVaRestCallDelegate Delegate;
};

DECLARE_MULTICAST_DELEGATE_FourParams(FOnSGLoginCompleteDelegate, int32, bool, const FUniqueNetId&, const FString&);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FSGSubsystemMessageDelegate, FSGOnlineFeatureSubsystemMessage, Message);

UCLASS()
class TALLGUYS_API USGOnlineFeaturePack : public UGameInstanceSubsystem
{
	GENERATED_BODY()

private:

	bool bWasInitialized = false;

	bool bIsPendingRefresh = false;
	
	TArray<FFrozeRequest> RequestQueue;
	
	UTGVaRestSubsystem* VaRestSubsystem;

	FOnLoginCompleteDelegate	OnLoginCompleteDelegate;
	FOnLogoutCompleteDelegate	OnLogoutCompleteDelegate;
	FVaRestCallDelegate			OnRefreshTokenCompleteDelegate;

	void LogResponse(UVaRestRequestJSON* RequestJson);

	
	
	bool CallRefreshAccessToken();
	UFUNCTION()
		void OnRefreshAccessTokenCompleted(UVaRestRequestJSON* RequestJson);


protected:

	FJWTObject AccessToken;
	FJWTObject RefreshToken;

	ESubsystemType CurrentLoginSubsystem;

	FVaRestCallDelegate AuthCallback;
	
	void SaveTokens();
    void LoadTokens();

	bool CallRequestAsAuthorizedClient(FSGRequestStamp& RequestInfo, const FVaRestCallDelegate& Callback);
	bool CallRequestWithTokenValidation(FSGRequestStamp& RequestInfo, const FVaRestCallDelegate& Callback);
	
	virtual void OnLoginComplete(int32 LocalUserNumber, bool IsSuccessful, const FUniqueNetId& UserID, const FString& Error);
	virtual void OnLogoutComplete(int32 LocalUserNumber, bool IsSuccessful);

public:
	UPROPERTY(BlueprintAssignable)
	FSGSubsystemMessageDelegate OnMessageAriseDelegate;
	
	virtual void Initialize(FSubsystemCollectionBase& Collection) override;

	bool TokenExpirationIsValid(FJWTObject Token);
	
	FString GetSubsystemName(ESubsystemType Subsystem);


	FOnSGLoginCompleteDelegate OnSGLoginComplete;

	UFUNCTION(BlueprintCallable)
		bool StartLoginWith(ESubsystemType ServiceProvider, const FVaRestCallDelegate& Callback);

	UFUNCTION(BlueprintCallable)
		bool Logout(ESubsystemType Subsystem);
	
	UFUNCTION(BlueprintCallable)
		bool CallRequest(const FSGRequestStamp& RequestInfo, const FVaRestCallDelegate& Callback);

	UFUNCTION(BlueprintCallable)
		FTGVersion GetTGVersion();

};
