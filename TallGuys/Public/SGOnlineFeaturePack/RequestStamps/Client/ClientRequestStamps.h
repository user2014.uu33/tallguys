#pragma once

#include "CoreMinimal.h"
#include "Engine/Engine.h"

#include "SGOnlineFeaturePack/RequestStamps/RequestStamps.h"

#include "ClientRequestStamps.generated.h"

//////////
///AUTH///
//////////

USTRUCT(BlueprintType)
struct FGoogleAuthContent
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString deviceId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString idToken;
};

USTRUCT(BlueprintType)
struct FFacebookAuthContent
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString deviceId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString accessToken;
};

USTRUCT(BlueprintType)
struct FAPIAuthOutput
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString accessToken;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString refreshToken;
};

USTRUCT(BlueprintType)
struct FAPIGoogleLoginRequestStamp : public FTGRequestStamp
{
	GENERATED_BODY()

	FAPIGoogleLoginRequestStamp() : Super()
	{
		Verb = EVaRestRequestVerb::POST;
		Path = "/auth/v{version}/Tokens/google";
	}
};

USTRUCT(BlueprintType)
struct FAPIFacebookLoginRequestStamp : public FTGRequestStamp
{
	GENERATED_BODY()

	FAPIFacebookLoginRequestStamp() : Super()
	{
		Verb = EVaRestRequestVerb::POST;
		Path = "/auth/v{version}/Tokens/facebook";
	}
};


////////////////////
///BATTLE RESULTS///
////////////////////

USTRUCT(BlueprintType)
struct FSeasonPassPremiumLevelUp
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString itemVariantId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 coinsQuantity = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 crystalsQuantity = 0;
};

USTRUCT(BlueprintType)
struct FSeasonPassLevelUp
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString itemVariantId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 coinsQuantity = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 crystalsQuantity = 0;      
};

USTRUCT(BlueprintType)
struct FBattleResult
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString itemVariantId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 coinsQuantity = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 crystalsQuantity = 0;      
};

USTRUCT(BlueprintType)
struct FReward
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FBattleResult battleResult;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FSeasonPassLevelUp seasonPassLevelUp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FSeasonPassPremiumLevelUp seasonPassPremiumLevelUp;      
};

USTRUCT(BlueprintType)
struct FResult
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 oldExperience;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 newExperience;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 oldLevel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 newLevel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FReward rewards;      
};

USTRUCT(BlueprintType)
struct FBattleResultOutput
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 state;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FResult results;      
};

USTRUCT(BlueprintType)
struct FGetBattleResultStamp : public FTGRequestStamp
{
	GENERATED_BODY()

	FGetBattleResultStamp() : Super()
	{
		Verb = EVaRestRequestVerb::GET;
		Path = "/game/v{version}/Battles/{id}/result";
	}
};


///////////////////
///GET ALL ITEMS///
///////////////////



USTRUCT(BlueprintType)
struct FTGVariant
{

	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString id;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString assetPath;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool isUsed;      
};

USTRUCT(BlueprintType)
struct FItem
{

	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString id;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 grade;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 costCoins;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 costCrystals;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 type;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString characterId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString seasonId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 marketCategory;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FTGVariant> variants;
};

USTRUCT(BlueprintType)
struct FGetAllItemsOutput
{

	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FItem> items;
};


USTRUCT(BlueprintType)
struct FGetAllItemsRequestStamp : public FTGRequestStamp
{
	GENERATED_BODY()

	FGetAllItemsRequestStamp() : Super()
	{
		Verb = EVaRestRequestVerb::GET;
		Path = "/game/v{version}/Items";
	}
};

/////////////////////
///BATTLE REQUESTS///
/////////////////////


USTRUCT(BlueprintType)
struct FTGItem
{

	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString id;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 grade;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 costCoins;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 costCrystals;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 type;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString characterId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString seasonId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 marketCategory;
};

USTRUCT(BlueprintType)
struct FOfferItemVariant
{

	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString id;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString assetPath;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTGItem item;
};

USTRUCT(BlueprintType)
struct FOffer
{

	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString id;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 category;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 segment;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FOfferItemVariant itemVariant;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 costCoins;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 costCrystals;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString expiration;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString givenTime;
};

USTRUCT(BlueprintType)
struct FGetMarketItemsOutput
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FOffer> offers;
};

USTRUCT(BlueprintType)
struct FGetMarketItemsRequestStamp : public FTGRequestStamp
{
	GENERATED_BODY()

	FGetMarketItemsRequestStamp() : Super()
	{
		Verb = EVaRestRequestVerb::GET;
		Path = "/game/v{version}/MarketItems";
	}
};


///////////////////
///PURCHASE ITEM///
///////////////////

USTRUCT(BlueprintType)
struct FBuyMarketItemRequestStamp : public FTGRequestStamp
{
	GENERATED_BODY()

	FBuyMarketItemRequestStamp() : Super()
	{
		Verb = EVaRestRequestVerb::POST;
		Path = "/game/v{version}/MarketItems/{id}/purchase";
	}
};

/////////////////
///SEASON INFO///
/////////////////


USTRUCT(BlueprintType)
struct FLevelReward
{

	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString seasonId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 level;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool isPremium;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString itemVariantId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 quantity;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 resourceType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString rewardId;
};

USTRUCT(BlueprintType)
struct FGetSeasonInfoOutput
{

	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString id;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite) 
	TArray<FLevelReward> levelRewards;
};


USTRUCT(BlueprintType)
struct FGetSeasonInfoRequestStamp : public FTGRequestStamp
{
	GENERATED_BODY()

	FGetSeasonInfoRequestStamp() : Super()
	{
		Verb = EVaRestRequestVerb::GET;
		Path = "/game/v{version}/Seasons/current";
	}
};

////////////////////
///GET USER ITEMS///
////////////////////


USTRUCT(BlueprintType)
struct FGetUserItemsOutput
{

	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString userId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FString> itemsIds;
};

USTRUCT(BlueprintType)
struct FGetUserItemsRequestStamp : public FTGRequestStamp
{
	GENERATED_BODY()

	FGetUserItemsRequestStamp() : Super()
	{
		Verb = EVaRestRequestVerb::GET;
		Path = "/game/v{version}/UserItems";
	}
};

//////////////////////
///APPLY USER ITEMS///
//////////////////////


USTRUCT(BlueprintType)
struct FApplyUserItemsContent
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FString> itemsIds;
};


USTRUCT(BlueprintType)
struct FApplyUserItemsOutput
{

	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString login;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 coins;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 crystals;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 experience;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 level;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool seasonPassPremium;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 roles;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FString> appliedItemsIds;
};


USTRUCT(BlueprintType)
struct FApplyUserItemsRequestStamp : public FTGRequestStamp
{
	GENERATED_BODY()

	FApplyUserItemsRequestStamp() : Super()
	{
		Verb = EVaRestRequestVerb::PUT;
		Path = "/game/v{version}/UserItems/apply";
	}
};

/////////////////////////////
///GET USER SEASON REWARDS///
/////////////////////////////


USTRUCT(BlueprintType)
struct FSeasonLevelReward
{

	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString seasonId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 level;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool isPremium;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString itemVariantId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 quantity;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 resourceType;
};

USTRUCT(BlueprintType)
struct FSeasonReward
{

	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString id;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString seasonLevelRewardId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FSeasonLevelReward seasonLevelReward;
};

USTRUCT(BlueprintType)
struct FGetUserSeasonRewardOutput
{

	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FSeasonReward> items;
};


USTRUCT(BlueprintType)
struct FGetUserSeasonRewardsRequestStamp : public FTGRequestStamp
{
	GENERATED_BODY()

	FGetUserSeasonRewardsRequestStamp() : Super()
	{
		Verb = EVaRestRequestVerb::GET;
		Path = "/game/v{version}/UserRewards";
	}
};

///////////////////////////////
///CLAIM USER SEASON REWARDS///
///////////////////////////////


USTRUCT(BlueprintType)
struct FClaimSeasonLevelReward
{

	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString seasonId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 level;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool isPremium;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString itemVariantId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 quantity;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 resourceType;
};

USTRUCT(BlueprintType)
struct FClaimSeasonReward
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString id;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString seasonLevelRewardId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FClaimSeasonLevelReward seasonLevelReward;
};

USTRUCT(BlueprintType)
struct FClaimUserSeasonRewardOutput
{

	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FClaimSeasonReward> items;
};


USTRUCT(BlueprintType)
struct FClaimUserSeasonRewardsRequestStamp : public FTGRequestStamp
{
	GENERATED_BODY()

	FClaimUserSeasonRewardsRequestStamp() : Super()
	{
		Verb = EVaRestRequestVerb::PUT;
		Path = "/game/v{version}/UserRewards/{id}/claim";
	}
};

/////////////////////
///CLAIM USER INFO///
/////////////////////


USTRUCT(BlueprintType)
struct FGetUserInfoOutput
{

	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString login;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 coins;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 crystals;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 experience;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 level;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool seasonPassPremium;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 roles;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FString> appliedItemsIds;
};


USTRUCT(BlueprintType)
struct FGetUserInfoRequestStamp : public FTGRequestStamp
{
	GENERATED_BODY()

	FGetUserInfoRequestStamp() : Super()
	{
		Verb = EVaRestRequestVerb::GET;
		Path = "/game/v{version}/Users";
	}
};


////////////////
///GET CONFIG///
////////////////


USTRUCT(BlueprintType)
struct FClientGetConfigRequestStamp : public FTGRequestStamp
{
	GENERATED_BODY()

	FClientGetConfigRequestStamp() : Super()
	{
		Verb = EVaRestRequestVerb::GET;
		Path = "/configs/v{version}/Configs/{id}";
	}
};


////////////////
///ASK BATTLE///
////////////////


USTRUCT(BlueprintType)
struct FAskBattleOutput
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString id;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 serverPort;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString serverIp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString accessToken;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool ready;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 expectedWaitingTimeSec;
};


USTRUCT(BlueprintType)
struct FAskBattleRequestStamp : public FTGRequestStamp
{
	GENERATED_BODY()

		FAskBattleRequestStamp() : Super()
	{
		Verb = EVaRestRequestVerb::GET;
		Path = "/queue/v{version}/BattleQueue/{battleId}";
	}
};

/////////////
///DEQUEUE///
/////////////


USTRUCT(BlueprintType)
struct FDequeuRequestStamp : public FTGRequestStamp
{
	GENERATED_BODY()

	FDequeuRequestStamp() : Super()
	{
		Verb = EVaRestRequestVerb::DEL;
		Path = "/queue/v{version}/BattleQueue/{battleId}";
	}
};

/////////////
///ENQUEUE///
/////////////


USTRUCT(BlueprintType)
struct FEnqueueTestBattleIdContent
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString battleType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 serverType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString testBattleId;
};

USTRUCT(BlueprintType)
struct FEnqueueContent
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString battleType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 serverType;
};


USTRUCT(BlueprintType)
struct FEnqueueOutput
{

	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString battleId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 expectedWaitingTimeSec;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 approximateCurrentUsersCount;
     
};


USTRUCT(BlueprintType)
struct FEnqueueRequestStamp : public FTGRequestStamp
{
	GENERATED_BODY()

		FEnqueueRequestStamp() : Super()
	{
		Verb = EVaRestRequestVerb::POST;
		Path = "/queue/v{version}/BattleQueue";
	}
};