#pragma once

#include "CoreMinimal.h"
#include "Engine/Engine.h"
#include "JsonObjectConverter.h"

#include "SGOnlineFeaturePack/VaRestHeaders/TGVaRestSubsystem.h"

#include "RequestStamps.generated.h"


#ifndef DEFINE_REQUEST_STAMP
#define DEFINE_REQUEST_STAMP
DECLARE_LOG_CATEGORY_CLASS(RequestStamp, Log, All)
//DEFINE_LOG_CATEGORY(RequestStamp)
#endif


USTRUCT(BlueprintType)
struct FSGRequestStamp
{
	GENERATED_BODY()

		struct ParamValue
	{
		FString Name;
		FString Value;
	};

	EVaRestRequestVerb Verb;
	FString URL;
	FString Path;
	TArray<ParamValue> PathParams;
	TArray<ParamValue> QueryParams;
	TArray<ParamValue> Headers;
	TSharedPtr<FJsonObject> ContentJson;

	FSGRequestStamp()
	{
		ContentJson = TSharedPtr<FJsonObject>(new FJsonObject());
	}

	void AddPathParam(FString Name, FString Value)
	{
		PathParams.Add({ Name, Value });
	}

	void AddQueryParam(FString Name, FString Value)
	{
		QueryParams.Add({ Name, Value });
	}

	void AddHeader(FString Name, FString Value)
	{
		Headers.Add({ Name, Value });
	}

	bool SetContent(UStruct* InStructType, const void* InStruct) const
	{
		return FJsonObjectConverter::UStructToJsonObject(InStructType, InStruct, ContentJson.ToSharedRef(), 0, 0);
	}

	FString GetEndPoint() const
	{
		FString OutEndpoint = URL;
		FString OutPath = Path;
		for (TArray<ParamValue>::TConstIterator It(PathParams); It; ++It)
		{
			OutPath = OutPath.Replace(*("{" + It->Name + "}"), *(It->Value), ESearchCase::CaseSensitive);
		}

		OutEndpoint += OutPath;
		if (QueryParams.Num() > 0)
		{
			OutEndpoint += "?";
			for (TArray<ParamValue>::TConstIterator It(QueryParams); It; ++It)
			{
				OutEndpoint += It->Name + "=" + It->Value;
				if ((It + 1))
				{
					OutEndpoint += "&";
				}
			}
		}

		return OutEndpoint;
	}
};

USTRUCT(BlueprintType)
struct FTGRequestStamp : public FSGRequestStamp
{
	GENERATED_BODY()

public:
	FTGRequestStamp() : Super()
	{
		if (!GConfig->GetString(TEXT("TGOnlineFeaturePack.CloudServer"), TEXT("APIUrl"), URL, GEngineIni))
		{
			UE_LOG(RequestStamp, Warning, TEXT("----- Missing APIUrl= in [TGOnlineFeaturePack.CloudServer] of DefaultGame.ini"));
		}
		
		//UE_LOG(RequestStamp, Display , TEXT("----- APIUrl = %s"), *URL);

		//Use requests version 1 by default
		AddPathParam("version", "1");
	}
};


//FTGRefreshAccessTokenStamp content struct
USTRUCT(BlueprintType)
struct FRefreshAccessTokenContent
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString refreshToken;
};

//FTGRefreshAccessTokenStamp output struct
USTRUCT(BlueprintType)
struct FRefreshAccessTokenOutput
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString accessToken;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString refreshToken;
};

USTRUCT(BlueprintType)
struct FTGRefreshAccessTokenStamp : public FTGRequestStamp
{
	GENERATED_BODY()

public:
	FTGRefreshAccessTokenStamp() : Super()
	{
		Verb = EVaRestRequestVerb::PUT;
		Path = "/auth/v{version}/Tokens/refresh";
	}
};