#pragma once

#include "CoreMinimal.h"
#include "Engine/Engine.h"

#include "SGOnlineFeaturePack/RequestStamps/RequestStamps.h"

#include "BattleServerRequestStamps.generated.h"


USTRUCT(BlueprintType)
struct FServerRequestStamp : public FTGRequestStamp
{
	GENERATED_BODY()
public:
	
	bool bUseInternalURL = false;
	
	
	FServerRequestStamp() : Super()
	{
		FName CLServerType;
		if(FParse::Value(FCommandLine::Get(), TEXT("SERVER_TYPE"), CLServerType))
		{
			if(CLServerType.IsEqual(TEXT("Remote")))
			{
				bUseInternalURL = true;
			}
		} 
		else
		{
			bUseInternalURL = true;
		}

		FString APIKey;
		if (!GConfig->GetString(TEXT("TGOnlineFeaturePack.CloudServer"), TEXT("APIKey"), APIKey, GEngineIni))
		{
			UE_LOG(RequestStamp, Warning, TEXT("----- Missing APIKey= in [TGOnlineFeaturePack.CloudServer] of DefaultGame.ini"));
		}
		AddHeader("tg-api-key", APIKey);
	}
};

USTRUCT(BlueprintType)
struct FGameRequestStamp : public FServerRequestStamp
{
	GENERATED_BODY()

public:
	FGameRequestStamp()
	{
		if (bUseInternalURL)
		{
			URL = "http://game-api-svc";
		}
	}
};

USTRUCT(BlueprintType)
struct FManagerRequestStamp : public FServerRequestStamp
{
	GENERATED_BODY()

public:
	FManagerRequestStamp()
	{
		if (bUseInternalURL)
		{
			URL = "http://manager-service-svc";
		}
	}
};

USTRUCT(BlueprintType)
struct FConfigRequestStamp : public FServerRequestStamp
{
	GENERATED_BODY()

public:
	FConfigRequestStamp()
	{
		if (bUseInternalURL)
		{
			URL = "http://configs-api-svc";
		}
	}
};

/////////////////////
///CONFIG REQUESTS///
/////////////////////

USTRUCT(BlueprintType)
struct FGetConfigOtput
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString id;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString content;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 format;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString updatedAt;      
};

//Returns config with info about config
USTRUCT(BlueprintType)
struct FGetConfigRequestStamp : public FConfigRequestStamp
{
	GENERATED_BODY()

	FGetConfigRequestStamp()
	{
		Verb = EVaRestRequestVerb::GET;
		Path = "/configs/v{version}/Configs/{id}";
	}
	
};

//Returns config content only
USTRUCT(BlueprintType)
struct FGetConfigContentRequestStamp : public FConfigRequestStamp
{
	GENERATED_BODY()

	FGetConfigContentRequestStamp()
	{
		Verb = EVaRestRequestVerb::GET;
		Path = "/bs/configs/v{version}/{id}/content";
	}
	
};

/////////////////////
///BATTLE REQUESTS///
/////////////////////


USTRUCT(BlueprintType)
struct FUsersDatum
{

	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString id;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString login;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FString> appliedItemsIds;      
};

USTRUCT(BlueprintType)
struct FBattleInfoOutput
{

	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString id;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString battleType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FUsersDatum> usersData;      
};


USTRUCT(BlueprintType)
struct FGetBattleInfoRequestStamp : public FGameRequestStamp
{
	GENERATED_BODY()

	FGetBattleInfoRequestStamp() : Super()
	{
		Verb = EVaRestRequestVerb::GET;
		Path = "/bs/game/v{version}/Battles/{id}";
	}
};

/////////////////////////
///POST BATTLE RESULTS///
/////////////////////////

USTRUCT(BlueprintType)
struct FRoundResults
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 roundNumber;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 achievement;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool isTeam;      
};

USTRUCT(BlueprintType)
struct FPostBattleResultsContent
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString userId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString battleId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 state;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FRoundResults> rounds;      
};


USTRUCT(BlueprintType)
struct FPostBattleResultsRequestStamp : public FGameRequestStamp
{
	GENERATED_BODY()

	FPostBattleResultsRequestStamp() : Super()
	{
		Verb = EVaRestRequestVerb::POST;
		Path = "/bs/game/v{version}/Battles/result";
	}
};

/////////////////////////////
///SET BATTLE SERVER STATE///
/////////////////////////////

USTRUCT(BlueprintType)
struct FUpdateBattleServerStateContent
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 state;
};


USTRUCT()
struct FUpdateBattleServerStateRequestStamp : public FManagerRequestStamp
{
	GENERATED_BODY()

	FUpdateBattleServerStateRequestStamp() : Super()
	{
		Verb = EVaRestRequestVerb::CUSTOM;
		Path = "/bs/manager-service/v{version}/BattleServers/{battleId}";
	}
};
