#pragma once

#include "CoreMinimal.h"
#include "Engine/Engine.h"

#include "RequestStructures.generated.h"

//USTRUCT(BlueprintType)
//struct FName
//{
//    GENERATED_BODY()
//public:
//    UPROPERTY(EditAnywhere, BlueprintReadWrite)
//        
//};


//
//ALL ITEMS START
// 

USTRUCT(BlueprintType)
struct FRSVariant
{
    GENERATED_BODY()
public:

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        FString id;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        FString name;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        FString assetPath;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        bool isUsed;
};

USTRUCT(BlueprintType)
struct FRSItem
{
    GENERATED_BODY()
public:
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        FString id;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        uint8 grade;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        int costCoins;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        int costCrystals;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        uint8 type;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        FString characterId;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        FString seasonId;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        uint8 marketCategory;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        TArray<FRSVariant> variants;
};

USTRUCT(BlueprintType)
struct FRSItems
{
    GENERATED_BODY()
public:
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        TArray<FRSItem> items;

};

//
//ALL ITEMS END
// 