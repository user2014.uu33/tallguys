﻿// Fill out your copyright notice in the Description page of Project Settings.


//TODO: macro DECLARE_HTTP_REQUEST_TOOLS(endpoint, name)
// add deserialization for structures
// Adding new http request:
// add json_response to json_ustruct schema
// add DECLARE_HTTP_REQUEST_TOOLS
// all works!!
//

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"

#include "Engine/Engine.h"
#include "SGOnlineFeaturePack.h"
#include "Misc/ConfigCacheIni.h"

#include "SGOnlineFeaturePack/RequestStamps/BattleServer/BattleServerRequestStamps.h"
#include "SGOnlineFeaturePack/RequestStamps/Client/ClientRequestStamps.h"
#include "Common/TGCommonStructures.h"
#include "RequestStructures/RequestStructures.h"
#include "Common/Save/TGSaveGame.h"

#include "Kismet/GameplayStatics.h"
#include "JsonObjectConverter.h"


#include "TGOnlineFeaturePack.generated.h"





DECLARE_LOG_CATEGORY_EXTERN(TGLogTGOnlineFeaturePack, Log, All);


UENUM()
enum class EMarketItemCategory : uint8
{
	UNAVAILABLE UMETA(DisplayName = "Unavailable"),
	USUAL UMETA(DisplayName = "Usual"),
	RARE UMETA(DisplayName = "Rare")
};

UENUM()
enum class EBattleType : uint8
{
	USUAL UMETA(DisplayName = "Usual")
};

USTRUCT(BlueprintType)
struct FRequestState
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bIsSuccess;

	//add err code here

};


DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnGetShopOffersCompleteDelegate, UVaRestJsonObject*, VarestJson, FRequestState, RequestState);


UCLASS()
class TALLGUYS_API UTGOnlineFeaturePack : public USGOnlineFeaturePack
{
	GENERATED_BODY()
	
private:

	bool bIsPendingLogin;
	
	FVaRestCallDelegate OnAuthCompleteDelegate;

	//OnLoginComplete for USGOnlineFeaturePack::Login(Facebook, Google...)
	virtual void OnLoginComplete(int32 LocalUserNumber, bool IsSuccessful, const FUniqueNetId& UserID, const FString& Error) override;
	
	UFUNCTION()
		void OnCloudServerAuthComplete(UVaRestRequestJSON* RequestJson);
	
public:
	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool RefreshTokenIsValid() { return TokenExpirationIsValid(RefreshToken); };

	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool AccessTokenIsValid() { return TokenExpirationIsValid(AccessToken); };

	UFUNCTION(BlueprintCallable)
		FPlayerAccountInfo GetPlayerAccount();

	
	UFUNCTION(BlueprintCallable)
		bool GetAllItems(const FVaRestCallDelegate& Callback);

	UFUNCTION(BlueprintCallable)
		bool GetMarketItems(EMarketItemCategory Category, const FVaRestCallDelegate& Callback);
	
	UFUNCTION(BlueprintCallable)
		bool GetUserItems(const FVaRestCallDelegate& Callback);

	UFUNCTION(BlueprintCallable)
		bool UserItemsApply(const TArray<FString> Ids, const FVaRestCallDelegate& Callback);

	UFUNCTION(BlueprintCallable)
		bool GetUser(const FVaRestCallDelegate& Callback);

	UFUNCTION(BlueprintCallable)
		bool BuyItem(FString OfferId, const FVaRestCallDelegate& Callback);

	UFUNCTION(BlueprintCallable)
		bool GetSeasonInfo(const FVaRestCallDelegate& Callback);
	
	UFUNCTION(BlueprintCallable)
		bool SendBattleResult(FPostBattleResultsContent UserInfo, const FVaRestCallDelegate& Callback);

	UFUNCTION(BlueprintCallable)
		bool GetBattleResult(FString BattleId, const FVaRestCallDelegate& Callback);

	//ServerType is "Usual"
	UFUNCTION(BlueprintCallable)
		bool Enqueue(FString BattleType, ETGServerType ServerType, FString TestBattleId, const FVaRestCallDelegate& Callback);

	UFUNCTION(BlueprintCallable)
		bool Dequeue(FString BattleId, const FVaRestCallDelegate& Callback);

	UFUNCTION(BlueprintCallable)
		bool AskBattleServer(FString BattleId, const FVaRestCallDelegate& Callback);


	UFUNCTION(BlueprintCallable)
		bool UpdateServerState(FString BattleId, EBattleServerState ServerState, const FVaRestCallDelegate& Callback);

	UFUNCTION(BlueprintCallable)
		bool GetBattleInfoById(FString BattleId, const FVaRestCallDelegate& Callback);

	UFUNCTION(BlueprintCallable)
		bool ApplySeasonReward(FString RewardId, const FVaRestCallDelegate& Callback);

	UFUNCTION(BlueprintCallable)
		bool GetConfigVersion(const FVaRestCallDelegate& Callback);


	
	UFUNCTION(BlueprintCallable)
		void PrintLogFileToLogTEST();

	
	UFUNCTION(BlueprintCallable)
		FString GetBattleIdFromCommandLine();



	UFUNCTION(BlueprintCallable)
		FAPIAuthOutput GetAPIAuthInfo(UVaRestRequestJSON* RequestJSON);

	UFUNCTION(BlueprintCallable)
		FBattleResultOutput GetBattleResultInfo(UVaRestRequestJSON* RequestJSON);

	UFUNCTION(BlueprintCallable)
		FGetAllItemsOutput GetAllItemsInfo(UVaRestRequestJSON* RequestJSON);

	UFUNCTION(BlueprintCallable)
		FGetMarketItemsOutput GetMarketItemsInfo(UVaRestRequestJSON* RequestJSON);

	UFUNCTION(BlueprintCallable)
		FGetSeasonInfoOutput GetSeasonInfoInfo(UVaRestRequestJSON* RequestJSON);
	
	UFUNCTION(BlueprintCallable)
		FGetUserItemsOutput GetUserItemsInfo(UVaRestRequestJSON* RequestJSON);

	UFUNCTION(BlueprintCallable)
		FApplyUserItemsOutput GetApplyedItemsInfo(UVaRestRequestJSON* RequestJSON);

	UFUNCTION(BlueprintCallable)
		FGetUserSeasonRewardOutput GetUserSeasonRewardInfo(UVaRestRequestJSON* RequestJSON);
	
	UFUNCTION(BlueprintCallable)
		FClaimUserSeasonRewardOutput GetClaimUserSeasonRewardInfo(UVaRestRequestJSON* RequestJSON);

	UFUNCTION(BlueprintCallable)
		FGetUserInfoOutput GetUserInfo(UVaRestRequestJSON* RequestJSON);
	
	UFUNCTION(BlueprintCallable)
		FEnqueueOutput GetEnqueueInfo(UVaRestRequestJSON* RequestJSON);

	UFUNCTION(BlueprintCallable)
		FAskBattleOutput GetAskBattleInfo(UVaRestRequestJSON* RequestJSON);

	UFUNCTION(BlueprintCallable)
		FBattleInfoOutput GetBattleInfo(UVaRestRequestJSON* RequestJSON);

};
