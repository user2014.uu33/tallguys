// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"

#include "Http.h"
#include "HttpModule.h"
#include "Interfaces/IHttpRequest.h"

#include "VaRestSubsystem.h"

#include "SGHttpCommunication.generated.h"

UENUM(BlueprintType)
enum class ERequestVerb : uint8
{
	GET			UMETA(DisplayName = "Get"),
	POST		UMETA(DisplayName = "Post"),
	PUT			UMETA(DisplayName = "Put"),
	DEL			UMETA(DisplayName = "Delete")
};

DECLARE_MULTICAST_DELEGATE_ThreeParams(FOnHttpResponseReceived, FHttpRequestPtr, FHttpResponsePtr, bool);

/** 
 *  
 */
UCLASS()
class TALLGUYS_API USGHttpCommunication : public UObject
{
	GENERATED_BODY()
	
private:
	FString GetVerbAsString(ERequestVerb Verb);
	EVaRestRequestVerb GetVerbAsVaRestVerb(ERequestVerb Verb);


public:
	FOnHttpResponseReceived OnHttpResponseDelegate;

	//UFUNCTION()
	void ProcessRequest(FString &Url, ERequestVerb Verb, FString Content);
	//UFUNCTION()
	void OnProcessRequestComplete(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
};
