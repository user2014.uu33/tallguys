// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TallGuys : ModuleRules
{
	public TallGuys(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] {
			"Core",
			"CoreUObject",
			"Engine",
			"InputCore",
			"PhysicsCore",
			"UMG",
			"VaRest",
			"LevelSequence"
		});

		PrivateDependencyModuleNames.AddRange(new string[] {
			"Json",
			"JsonUtilities",
			"Http",
			"OnlineSubsystem"
		});
		
		if (Target.Platform == UnrealTargetPlatform.Android)
		{
			string PluginPath = Utils.MakePathRelativeTo(ModuleDirectory, Target.RelativeEnginePath);
			AdditionalPropertiesForReceipt.Add("AndroidPlugin", System.IO.Path.Combine(PluginPath, "TG_UPL.xml"));
			
			DynamicallyLoadedModuleNames.Add("OnlineSubsystemGooglePlay");
		}

		if (Target.Type == TargetRules.TargetType.Game)
		{
			PrivateDependencyModuleNames.AddRange(new string[] {
				"OnlineSubsystemFacebook",
				"OnlineSubsystemGoogle"
			});
		}
	}
}
