// Fill out your copyright notice in the Description page of Project Settings.



// Copyright Epic Games, Inc. All Rights Reserved.

#include "Character/PhysBasedMovementComponent.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/Controller.h"
#include "GameFramework/WorldSettings.h"
#include "Character/PhysBasedCharacter.h"
#include "Components/CapsuleComponent.h"
#include "DrawDebugHelpers.h"

#include "Net/UnrealNetwork.h"

#include "Components/PrimitiveComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Chaos/ChaosEngineInterface.h"
#include "GameFramework/PlayerState.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h"

#include "Misc/AssertionMacros.h"


////////////////////////////
//PhysChar definitions begin
////////////////////////////


void UPhysBasedMovementComponent::StartJump()
{
	if (IfCanJump())
	{
		FVector FinalJumpVelocity = FVector(0.0f, 0.0f, JumpVelocity);
		UpdatedCapsuleComponent->SetPhysicsLinearVelocity(FinalJumpVelocity);
		IsJump = true;
		bIsJump = true;
	}
}

void UPhysBasedMovementComponent::StartLongJump()
{
	if (GetMovementMode() == EPhysBasedMovementMode::CLIMBING)
	{
		FinishClimbing();
	}

	if (IsCanLongJump == false)
	{
		return;
	}

	if (IfCanLongJump())
	{
		FVector FinalLongJumpVelocity;

		FVector LongJumpVelocityVector = UpdatedCapsuleComponent->GetForwardVector() * LongJumpVelocity;
		FVector InitVelocitySum = Velocity + UpdatedCapsuleComponent->GetComponentVelocity();
		float VelocitySumSize = InitVelocitySum.Size2D();

		FinalLongJumpVelocity = InitVelocitySum + LongJumpVelocityVector;
		float FinalLongJumpSize = FinalLongJumpVelocity.Size2D();

		float MoreMaxVelocitySize = MaxVelocitySize + MaxVelocitySizeOffset;

		if (FinalLongJumpVelocity.Size2D() > MoreMaxVelocitySize + 100)
		{
			if ((FinalLongJumpSize - VelocitySumSize) > 0)
			{
				if (VelocitySumSize < MoreMaxVelocitySize)
				{
					FinalLongJumpVelocity = FinalLongJumpVelocity.GetClampedToMaxSize2D(VelocitySumSize);
				}
				else
				{
					float DotProduct = FVector::DotProduct(InitVelocitySum.GetSafeNormal2D(), LongJumpVelocityVector.GetSafeNormal2D());
					float ProjectionMul = (1 - ((FinalLongJumpSize - MoreMaxVelocitySize) / LongJumpVelocityVector.Size2D())) * DotProduct;
					FVector LongJumpVelocityProjection = LongJumpVelocityVector * ProjectionMul;
					FinalLongJumpVelocity = InitVelocitySum + LongJumpVelocityProjection;
				}
			}
		}

		SetMovementMode(EPhysBasedMovementMode::LONGJUMPING);

		FinalLongJumpVelocity += FVector(0.0f, 0.0f, 100.0f);

		UpdatedCapsuleComponent->SetPhysicsLinearVelocity(FinalLongJumpVelocity);
		//IsCanLongJump = false;
		bIsLongJump = true;
	}
}

void UPhysBasedMovementComponent::FinishClimbing()
{
	UE_LOG(LogTemp, Warning, TEXT("Climbing! %d"), UPhysBasedMovementComponent::GetOwner()->GetNetMode());
	FVector Newlocation;
	FVector DirVector = UpdatedCapsuleComponent->GetForwardVector() * 150;

	Newlocation.X = GetActorLocation().X + DirVector.X;
	Newlocation.Y = GetActorLocation().Y + DirVector.Y;
	Newlocation.Z = GetActorLocation().Z + 200;
	UpdatedCapsuleComponent->SetRelativeLocation(Newlocation, false, 0, ETeleportType::None);

	UpdatedCapsuleComponent->SetEnableGravity(true);
	SetMovementMode(EPhysBasedMovementMode::WALKING);
	IsJump = false;

}

void UPhysBasedMovementComponent::SetMovementMode(EPhysBasedMovementMode Mode)
{
	if (GetOwner()->GetActorTransform().ContainsNaN())
	{
		return;
	}

	if (Mode == EPhysBasedMovementMode::WALKING)
	{
		if (GetMovementMode() == EPhysBasedMovementMode::LONGJUMPING)
		{
			UpdatedCapsuleComponent->SetConstraintMode(EDOFMode::None);

			StartStandingUp();
		}
		if (GetMovementMode() == EPhysBasedMovementMode::FALLING)
		{
			UpdatedCapsuleComponent->SetConstraintMode(EDOFMode::None);
			FRotator CapsuleRelativeRotation = UpdatedCapsuleComponent->GetRelativeRotation();
			FRotator NewRotation = FRotator(0.0f, CapsuleRelativeRotation.Yaw, 0.0f);
			UpdatedCapsuleComponent->SetRelativeRotation(NewRotation, true);
			bIsLongJumpInProcess = false;

		}


		UpdatedCapsuleComponent->SetConstraintMode(EDOFMode::Default);
		CurrTimeToStandUp = 0.0F;

		//GetOwner()->SetReplicateMovement(false);
	}
	else if (Mode == EPhysBasedMovementMode::JUMPING)
	{

	}
	else if (Mode == EPhysBasedMovementMode::CLIMBING)
	{
		//CurrMovementMode = Mode;
	}
	else if (Mode == EPhysBasedMovementMode::FALLING)
	{
		UpdatedCapsuleComponent->SetConstraintMode(EDOFMode::None);
		CurrTimeToStandUp = TTSFall;
		//GetOwner()->SetReplicateMovement(true);

	}
	else if (Mode == EPhysBasedMovementMode::LONGJUMPING)
	{

		UpdatedCapsuleComponent->SetConstraintMode(EDOFMode::None);
		StartPikeMove();
		UpdatedCapsuleComponent->SetConstraintMode(EDOFMode::Default);

		CurrTimeToStandUp = TTSLongJump;

		//GetOwner()->SetReplicateMovement(true);

	}

	CurrMovementMode = Mode;
}

UPhysBasedMovementComponent::UPhysBasedMovementComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	SetIsReplicatedByDefault(true);

	MaxSpeed = 450.f;
	Acceleration = 700.f;
	Deceleration = 1000.f;
	TurningBoost = 8.0f;
	MaxSurfaceAngleToStep = 45.0f;
	MaxUpdatedComponentVelocity = 5000.0f;
	AdditionalImpulseMul = 0.3f;
	ImpulseToFall = 250000.0f;
	LongJumpVelocity = 500.0f;
	JumpVelocity = 700.0f;
	MaxVelocitySize = 700.0f;
	MaxVelocitySizeOffset = 10.0f;
	SlideDownAcceleration = 4000.0f;
	TTSLongJump = 0.7f;
	TTSFall = 2.0f;

	InAirImpulseToFall = 125000.0f;
	InAirAccelerationMul = 0.5f;
	InAirDecelerationMul = 0.1f;
	InAirTTSMul = 1.0f;

	SlimeAccelerationMul = 0.5f;
	SlimeDecelerationMul = 0.3f;
	SlimeSlideDownAcceleration = 1000.0f;

	bPositionCorrected = false;

	CharacterOwner = Cast<APhysBasedCharacter>(GetOwner());



	ResetMoveState();
}

void UPhysBasedMovementComponent::OnRegister()
{
	Super::OnRegister();

	if (UpdatedComponent)
	{
		UpdatedCapsuleComponent = Cast<UCapsuleComponent>(UpdatedComponent);
	}
	if (UpdatedCapsuleComponent)
	{
		UpdatedCapsuleComponent->OnComponentHit.AddDynamic(this, &UPhysBasedMovementComponent::OnUpdatedComponentHit);
	}
}

void UPhysBasedMovementComponent::OnCreatePhysicsState()
{
	Super::OnCreatePhysicsState();
	UE_LOG(LogTemp, Warning, TEXT("----Call OnCreatePhysicsState()----"))
		if (UpdatedCapsuleComponent)
		{
			UpdatedCapsuleComponent->OnComponentHit.AddDynamic(this, &UPhysBasedMovementComponent::OnUpdatedComponentHit);
		}
}

void UPhysBasedMovementComponent::GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(UPhysBasedMovementComponent, CurrMovementMode);
}

void UPhysBasedMovementComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	//TickClimbingSystem();

	if (ShouldSkipUpdate(DeltaTime))
	{
		return;
	}

	if (!GetOwner()->GetActorTransform().IsValid() || !GetOwner()->GetRootComponent()->GetRelativeTransform().IsValid())
	{
		bNaNAppeared = true;
		OnNeedRespawn.Broadcast();
		return;
	}

	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!PawnOwner || !UpdatedComponent)
	{
		LimitWorldBounds();
		return;
	}


	FindFloor();

	ApplyControlInputToVelocity(DeltaTime);


	bPositionCorrected = false;

	// Move actor
	FVector Delta = Velocity * DeltaTime;

	if (!Delta.IsNearlyZero(1e-6f))
	{
		const FVector OldLocation = UpdatedComponent->GetComponentLocation();

		const FQuat Rotation = UpdatedComponent->GetComponentQuat();
		//const FQuat Rotation = UKismetMathLibrary::MakeRotFromX(Velocity).Quaternion();

		FHitResult Hit(1.0f);

		bool SafeMove = true;

		if (GetMovementMode() == EPhysBasedMovementMode::FALLING)
		{
			SafeMove = false;
		}

		if (SafeMove)
		{
			if (GetMovementMode() == EPhysBasedMovementMode::WALKING)
			{
				if (GetOwnerRole() != ROLE_AutonomousProxy && GetNetMode() == ENetMode::NM_Client)
				{
					if (bNeedAdjustment)
					{
						MoveDelta = Delta;
						AdjustmentDelta = TargetAdjustmentLocation - UpdatedComponent->GetComponentLocation();
						Delta = FMath::Lerp<FVector, float>(Delta, TargetAdjustmentLocation - UpdatedComponent->GetComponentLocation(), 0.03);

					}
				}

				SafeMoveUpdatedComponent(Delta, Rotation, true, Hit);

			}
		}


		if (Hit.IsValidBlockingHit())
		{
			HandleImpact(Hit, DeltaTime, Delta);
			// Try to slide the remaining distance along the surface.
			SlideAlongSurface(Delta, 1.f - Hit.Time, Hit.Normal, Hit, true);
		}

		// Update velocity
		// We don't want position changes to vastly reverse our direction (which can happen due to penetration fix ups etc)
		if (!bPositionCorrected)
		{
			const FVector NewLocation = UpdatedComponent->GetComponentLocation();
			Velocity = ((NewLocation - OldLocation) / DeltaTime);
		}

	}
	//UpdatedComponent->ComponentVelocity;
	// Finalize

	UpdateComponentVelocity();
	UpdateMovementMode(DeltaTime);

	ClampPhysVelocity();


	//if we are owner client send movement data to server
	if (GetOwnerRole() == ROLE_AutonomousProxy)
	{
		if (PawnOwner->GetController() == UGameplayStatics::GetPlayerController(GetWorld(), 0))
		{
			ServerMove();
		}
	}


	if (GetNetMode() == ENetMode::NM_DedicatedServer)
	{
		AController* Controller = PawnOwner->GetController();
		//resolve ue crash , need refact , make check if our pawn proccessed only by server nor if it is AI 
		if (Controller)
		{
			//if we are player character
			if (Controller->IsPlayerController())
			{
				bool bIsValid = ValidateClientMove();

				if (bIsValid)
				{
					ClientMoveInfo.bIsClientMoveValid = true;
					ClientMoveInfo.MovementMode = (uint8)GetMovementMode();
					ClientMoveInfo.bIsJump = bIsJump;
					ClientMoveInfo.bIsLongJump = bIsLongJump;

					CharacterOwner->ClientServerMoveResponse(ClientMoveInfo);
					CharacterOwner->MulticastMove(ClientMoveInfo);
				}
				else
				{
					ClientMoveInfo.bIsClientMoveValid = false;


					//if not valid use server location and rotation
					ClientMoveInfo.NewLocation = PawnOwner->GetActorLocation();
					ClientMoveInfo.NewRotation = PawnOwner->GetActorRotation().Quaternion();
					ClientMoveInfo.MovementMode = (uint8)GetMovementMode();
					ClientMoveInfo.bIsJump = bIsJump;
					ClientMoveInfo.bIsLongJump = bIsLongJump;

					CharacterOwner->ClientServerMoveResponse(ClientMoveInfo);
					CharacterOwner->MulticastMove(ClientMoveInfo);
				}
			}
			else //else if we AI character
			{
				FPhysCharMoveInfo AIMoveInfo;

				AIMoveInfo.bIsClientMoveValid = true;
				AIMoveInfo.LastForwardInputVector = LastForwardInputVector;
				AIMoveInfo.LastRightInputVector = LastRightInputVector;
				AIMoveInfo.MovementMode = (uint8)GetMovementMode();
				AIMoveInfo.NewLocation = PawnOwner->GetActorLocation();
				AIMoveInfo.NewRotation = PawnOwner->GetActorRotation().Quaternion();
				AIMoveInfo.bIsJump = bIsJump;
				AIMoveInfo.bIsLongJump = bIsLongJump;

				CharacterOwner->MulticastMove(AIMoveInfo);
			}
		}

	}

	bIsJump = false;
	bIsLongJump = false;
}

bool UPhysBasedMovementComponent::ValidateClientMove()
{

	FVector ClientLoc = ClientMoveInfo.NewLocation;
	FVector ServerLoc = PawnOwner->GetActorLocation();

	bool bIsValid = ClientLoc.Equals(ServerLoc, 10.0F);

	if (!bIsValid)
	{
		PawnOwner->SetActorLocationAndRotation(ClientLoc, PawnOwner->GetActorRotation(), true); //TEMP!!!
		NeedServerAdjustmentTickCount++;
	}
	else
	{
		NeedServerAdjustmentTickCount = 0;
	}

	if (NeedServerAdjustmentTickCount > 300)
	{
		PawnOwner->SetActorLocationAndRotation(ClientLoc, PawnOwner->GetActorRotation(), false); //TEMP!!!
	}

	//UE_LOG(LogTemp, Warning, TEXT("%f"), FMath::Abs(ClientLoc.Size() - ServerLoc.Size()));

	return true;//bIsValid;

}

void UPhysBasedMovementComponent::UpdateMovementMode(float DeltaTime)
{

	if (CurrTimeToStandUp > 0.0F)
	{
		CurrTimeToStandUp -= DeltaTime * InAirTTSMul;
	}
	else if (GetMovementMode() != EPhysBasedMovementMode::WALKING)
	{
		if (GetMovementMode() == EPhysBasedMovementMode::CLIMBING)
		{
			SetMovementMode(EPhysBasedMovementMode::CLIMBING);
			//return;
		}
		else
		{
			SetMovementMode(EPhysBasedMovementMode::WALKING);
			//StartStandingUp();
		}
	}
}

void UPhysBasedMovementComponent::StartStandingUp()
{
	FHitResult SweepHitResult;

	//FRotator CapsuleRelativeRotation = UpdatedCapsuleComponent->GetRelativeRotation();
	//FRotator NewRotation = FRotator(0.0f, CapsuleRelativeRotation.Yaw, 0.0f);
	//FQuat NewRotationQuat = NewRotation.Quaternion();

	//FVector ForwardOffset = UpdatedCapsuleComponent->GetForwardVector() * UpdatedCapsuleComponent->GetScaledCapsuleHalfHeight();
	//FVector FinalOffset = FVector(ForwardOffset.X, ForwardOffset.Y, -(UpdatedCapsuleComponent->GetScaledCapsuleHalfHeight() - UpdatedCapsuleComponent->GetScaledCapsuleRadius()));
	//FVector NewLocation = UpdatedCapsuleComponent->GetComponentLocation() - FinalOffset;


	//UpdatedCapsuleComponent->SetRelativeTransform(FTransform(NewRotationQuat, NewLocation, FVector::OneVector), true, &SweepHitResult);

	UpdatedCapsuleComponent->SetConstraintMode(EDOFMode::None);
	FRotator CapsuleRelativeRotation = UpdatedCapsuleComponent->GetRelativeRotation();
	FRotator NewRotation = FRotator(0.0f, CapsuleRelativeRotation.Yaw, 0.0f);
	UpdatedCapsuleComponent->SetRelativeRotation(NewRotation, true, &SweepHitResult);

	if (SweepHitResult.bBlockingHit || SweepHitResult.bStartPenetrating)
	{
		SetMovementMode(EPhysBasedMovementMode::FALLING);
	}
	bIsLongJumpInProcess = false;
}

void UPhysBasedMovementComponent::StartPikeMove()
{
	FHitResult SweepHitResult;

	FRotator NewRotation = UpdatedCapsuleComponent->GetRelativeRotation().Add(-90, 0, 0);

	FVector ForwardOffset = UpdatedCapsuleComponent->GetForwardVector() * UpdatedCapsuleComponent->GetScaledCapsuleHalfHeight();
	float DownwardOffset = -(UpdatedCapsuleComponent->GetScaledCapsuleHalfHeight() - UpdatedCapsuleComponent->GetScaledCapsuleRadius());
	FVector FinalOffset = FVector(ForwardOffset.X, ForwardOffset.Y, DownwardOffset);
	FVector NewLocation = UpdatedCapsuleComponent->GetComponentLocation() + FinalOffset;

	UpdatedCapsuleComponent->SetRelativeTransform(FTransform(NewRotation, NewLocation, FVector::OneVector), true, &SweepHitResult);
	bIsLongJumpInProcess = true;
}

void UPhysBasedMovementComponent::ClampPhysVelocity()
{
	if (UpdatedCapsuleComponent)
	{
		if (UpdatedCapsuleComponent->GetComponentVelocity().Size() > MaxUpdatedComponentVelocity)
		{
			FVector ClampedVelocity = UpdatedCapsuleComponent->GetComponentVelocity().GetClampedToMaxSize(MaxUpdatedComponentVelocity);
			UpdatedCapsuleComponent->SetPhysicsLinearVelocity(ClampedVelocity);
		}
	}
}

bool UPhysBasedMovementComponent::IfCanJump()
{
	bool bIfCanJump = GetMovementMode() == EPhysBasedMovementMode::WALKING && IfOnFloor();//
	if (!IfOnFloor() && !bool(EPhysBasedMovementMode::WALKING))
		bIfCanJump = GetMovementMode() == EPhysBasedMovementMode::CLIMBING;
	return bIfCanJump;
}


bool UPhysBasedMovementComponent::IfCanLongJump()
{
	bool bIfCanLongJump = GetMovementMode() != EPhysBasedMovementMode::FALLING && GetMovementMode() != EPhysBasedMovementMode::LONGJUMPING && !IfOnFloor();
	//for the check on trace hit
	IsCanLongJump = true;

	return bIfCanLongJump;
}

void UPhysBasedMovementComponent::OnUpdatedComponentHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	//if (GetNetMode() == ENetMode::NM_DedicatedServer)
	{
		float CurrImpulseToFall = (bIsOnFloor) ? (ImpulseToFall) : (InAirImpulseToFall);

		EPhysicalSurface HitSurface = UPhysicalMaterial::DetermineSurfaceType(Hit.PhysMaterial.Get());

		if (HitSurface != SURFACE_NonFallSurface)
		{
			if (NormalImpulse.Size() >= CurrImpulseToFall)
			{
				//UE_LOG(LogTemp, Warning, TEXT("Impusle: %f"), NormalImpulse.Size());
				SetMovementMode(EPhysBasedMovementMode::FALLING);
				//UpdatedCapsuleComponent->AddForce(NormalImpulse * AdditionalImpulseMul);
			}
		}
	}
}

void UPhysBasedMovementComponent::ServerMove()
{

	FPhysCharMoveInfo MoveInfo;

	MoveInfo.LastForwardInputVector = LastForwardInputVector;
	MoveInfo.LastRightInputVector = LastRightInputVector;
	MoveInfo.NewLocation = PawnOwner->GetActorLocation();
	MoveInfo.NewRotation = PawnOwner->GetActorRotation().Quaternion();


	//UE_LOG(LogTemp, Warning, TEXT("%d, %d"), GetNetMode(), MoveInfo.LastForwardInputVector.X);

	if (CharacterOwner)
	{
		CharacterOwner->ServerMove(MoveInfo);
	}

	/*APlayerController* PC = Cast<APlayerController>(PawnOwner->GetController());
	if (PC)
	{
		APlayerState* PS = PC->GetPlayerState<APlayerState>();
		if (PS)
		{

			if (GEngine)
				GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow,
					FString::Printf(TEXT("%i, %i: %f, %f, %f"),
						GetNetMode(), PS->GetPlayerId(), MoveInfo.LastInputVector.X, MoveInfo.LastInputVector.Y, MoveInfo.LastInputVector.Z));

		}
	}*/
}



void UPhysBasedMovementComponent::HandleServerMove(FPhysCharMoveInfo MoveInfo)
{
	if (CharacterOwner)
	{
		LastForwardInputVector = MoveInfo.LastForwardInputVector;
		LastRightInputVector = MoveInfo.LastRightInputVector;
		ClientMoveInfo = MoveInfo;

		//UE_LOG(LogTemp, Warning, TEXT("%d, %d"), GetNetMode(), MoveInfo.LastForwardInputVector.X);		

	}
}

void UPhysBasedMovementComponent::HandleMulticastMove(FPhysCharMoveInfo MoveInfo)
{

	if (GetOwnerRole() != ROLE_AutonomousProxy)
	{

		if (GetMovementMode() != (EPhysBasedMovementMode)MoveInfo.MovementMode && GetMovementMode() != EPhysBasedMovementMode::LONGJUMPING)
		{
			SetMovementMode((EPhysBasedMovementMode)MoveInfo.MovementMode);
		}

		LastForwardInputVector = MoveInfo.LastForwardInputVector;
		LastRightInputVector = MoveInfo.LastRightInputVector;

		HandleNewClientLocation(MoveInfo.NewLocation, MoveInfo.NewRotation);

		PawnOwner->GetActorLocation();

		if (MoveInfo.bIsJump)
		{
			StartJump();
		}

		if (MoveInfo.bIsLongJump)
		{
			StartLongJump();
		}
	}

}

void UPhysBasedMovementComponent::HandleNewClientLocation(FVector NewLocation, FQuat NewRotation)
{
	if (GetNetMode() != ENetMode::NM_DedicatedServer)
	{
		if (!PawnOwner->GetActorLocation().Equals(NewLocation, 10))
		{
			if (!PawnOwner->GetActorLocation().Equals(NewLocation, 1300))
			{
				PawnOwner->SetActorLocation(NewLocation);
			}

			TargetAdjustmentLocation = NewLocation;//do smooth interpret of location if Pawnloc != NewLoc
			bNeedAdjustment = true;
			PawnOwner->SetActorRotation(NewRotation);
		}
	}
}

void UPhysBasedMovementComponent::HandleServerResponse(FPhysCharMoveInfo MoveInfo)
{
	if (GetOwnerRole() == ROLE_AutonomousProxy)
	{
		if (!MoveInfo.bIsClientMoveValid)
		{
			PawnOwner->SetActorLocationAndRotation(MoveInfo.NewLocation, MoveInfo.NewRotation, true);
		}

	}
}

void UPhysBasedMovementComponent::SetForwardInputVector(FVector Input, float Scale)
{
	LastForwardInputVector = Input * Scale;
}

void UPhysBasedMovementComponent::SetRightInputVector(FVector Input, float Scale)
{
	LastRightInputVector = Input * Scale;
}

//void UPhysBasedMovementComponent::SetForwardInputVector(FVector Input, float Scale)
//{
//}

bool UPhysBasedMovementComponent::LimitWorldBounds()
{
	AWorldSettings* WorldSettings = PawnOwner ? PawnOwner->GetWorldSettings() : NULL;
	if (!WorldSettings || !WorldSettings->bEnableWorldBoundsChecks || !UpdatedComponent)
	{
		return false;
	}

	const FVector CurrentLocation = UpdatedComponent->GetComponentLocation();
	if (CurrentLocation.Z < WorldSettings->KillZ)
	{
		Velocity.Z = FMath::Min(GetMaxSpeed(), WorldSettings->KillZ - CurrentLocation.Z + 2.0f);
		return true;
	}

	return false;
}

void UPhysBasedMovementComponent::ApplyControlInputToVelocity(float DeltaTime)
{
	bool bShouldUseTurningBoost = true;
	EPhysicalSurface SurfaceType = UPhysicalMaterial::DetermineSurfaceType(FloorResult.FloorHitResult.PhysMaterial.Get());

	FVector ControlAcceleration = GetInputVector().GetClampedToMaxSize(1.f);//
	const float AnalogInputModifier = (ControlAcceleration.SizeSquared() > 0.f ? ControlAcceleration.Size() : 0.f);
	const float MaxPawnSpeed = GetMaxSpeed() * AnalogInputModifier;

	float PawnAcceleration = Acceleration;
	float PawnDeceleration = Deceleration;


	if (!bIsOnFloor)
	{
		bShouldUseTurningBoost = false;
		PawnAcceleration *= InAirAccelerationMul;
		PawnDeceleration *= InAirDecelerationMul;
	}

	if (SurfaceType == SURFACE_Slime)
	{
		bShouldUseTurningBoost = false;
		PawnAcceleration *= SlimeAccelerationMul;
		PawnDeceleration *= SlimeDecelerationMul;
	}


	//Add velocity input change (acceleration)
	if (AnalogInputModifier > KINDA_SMALL_NUMBER)
	{
		if (bShouldUseTurningBoost)
		{
			Velocity = Velocity + (ControlAcceleration * Velocity.Size() - Velocity) * DeltaTime * TurningBoost;
		}

		Velocity += ControlAcceleration * PawnAcceleration * DeltaTime;
		Velocity = Velocity.GetClampedToMaxSize(MaxPawnSpeed);
	}
	else
	{
		if (Velocity.SizeSquared() > KINDA_SMALL_NUMBER)
		{
			FVector NewVelocity = Velocity.GetSafeNormal() * PawnDeceleration * DeltaTime;
			if (NewVelocity.SizeSquared() >= Velocity.SizeSquared())
			{
				Velocity = FVector::ZeroVector;
			}
			else
			{
				Velocity -= NewVelocity;
			}
		}
	}


	//Add slide down velocity if need (if surface angle is not standable)
	if (bIsOnFloor)
	{
		float HitNormalXYSize = FloorResult.FloorHitResult.ImpactNormal.Size2D();
		float SurfaceAngle = FMath::RadiansToDegrees(FMath::Asin(HitNormalXYSize));

		if (SurfaceAngle >= MaxSurfaceAngleToStep)
		{
			FVector NormalXY = FVector(FloorResult.FloorHitResult.ImpactNormal.X, FloorResult.FloorHitResult.ImpactNormal.Y, 0).GetSafeNormal();

			Velocity += NormalXY * SlideDownAcceleration * DeltaTime;
		}

		if (SurfaceType == SURFACE_Slime)
		{

			FVector NormalXY = FVector(FloorResult.FloorHitResult.ImpactNormal.X, FloorResult.FloorHitResult.ImpactNormal.Y, 0);

			if (NormalXY.SizeSquared() >= KINDA_SMALL_NUMBER)

				Velocity += NormalXY * SlimeSlideDownAcceleration * DeltaTime;

		}
	}

	//Calc velocity Z delta to get parallel to floor
	//We have ComputeSlideVector(...) but it dont work as expected
	if (bIsOnFloor)
	{
		FVector HitNormalXY = { FloorResult.FloorHitResult.ImpactNormal.X, FloorResult.FloorHitResult.ImpactNormal.Y, 0 };
		FVector VelocityXY = { Velocity.X, Velocity.Y, 0 };
		float HitNormalXYSize = HitNormalXY.Size2D();
		float VelocityXYSize = Velocity.Size2D();
		float HitNormalZSize = FloorResult.FloorHitResult.ImpactNormal.Z;

		float VelNormalDotProd = -FVector::DotProduct(VelocityXY.GetSafeNormal(), HitNormalXY.GetSafeNormal());

		Velocity.Z = (VelNormalDotProd * HitNormalXYSize * VelocityXYSize / HitNormalZSize);
	}

}

void UPhysBasedMovementComponent::FindFloor()
{
	UCapsuleComponent* CapsuleComponent = Cast<UCapsuleComponent>(UpdatedComponent);
	if (!CapsuleComponent)
	{
		return;
	}

	float TraceDistance = CapsuleComponent->GetScaledCapsuleHalfHeight() * 1.1;
	float TraceHalfHeight = CapsuleComponent->GetScaledCapsuleHalfHeight() / 2;
	float TraceRadius = CapsuleComponent->GetScaledCapsuleRadius() / 2;

	FVector StartTrace = UpdatedComponent->GetComponentLocation();
	FVector EndTrace = StartTrace - FVector(0.0f, 0.0f, TraceDistance);

	FCollisionShape CapsuleShape = FCollisionShape::MakeCapsule(TraceRadius, TraceHalfHeight);

	ECollisionChannel CollisionChannel = UpdatedComponent->GetCollisionObjectType();
	FCollisionQueryParams QueryParams(SCENE_QUERY_STAT(ComputeFloorDist), false, CharacterOwner);
	FCollisionResponseParams ResponseParam;
	InitCollisionParams(QueryParams, ResponseParam);

	//if bTraceComplex == true SweepSingleByChannel cannot find phys mat (WTF???)
	QueryParams.bTraceComplex = false;

	/*if (bIsOnFloor)
	{
		return;
	}*/
	bIsOnFloor = GetWorld()->SweepSingleByChannel(FloorResult.FloorHitResult,
		StartTrace, EndTrace, FQuat::Identity,
		CollisionChannel, CapsuleShape,
		QueryParams, ResponseParam);
}

void UPhysBasedMovementComponent::UpdateBasedMovement(float DeltaTime)
{

	/*UCapsuleComponent* CapsuleComponent = Cast<UCapsuleComponent>(UpdatedComponent);
	if (!CapsuleComponent)
	{
		return;
	}


	FQuat NewBaseRotation;
	FVector NewBaseLocation;

	UPrimitiveComponent* MovementBase = FloorResult.FloorHitResult.Component.Get();
	if (!MovementBase)
	{
		return;
	}

	NewBaseRotation = MovementBase->GetComponentQuat();
	NewBaseLocation = MovementBase->GetComponentLocation();

	FQuatRotationTranslationMatrix OldLocalToWorld(OldBaseRotation, OldBaseLocation);
	FQuatRotationTranslationMatrix NewLocalToWorld(NewBaseRotation, NewBaseLocation);

	FQuat FinalQuat = UpdatedComponent->GetComponentQuat();

	FVector const BaseOffset(0.0f, 0.0f, CapsuleComponent->GetScaledCapsuleHalfHeight());
	FVector const LocalBasePos = OldLocalToWorld.InverseTransformPosition(UpdatedComponent->GetComponentLocation() - BaseOffset);
	FVector const NewWorldPos = ConstrainLocationToPlane(NewLocalToWorld.TransformPosition(LocalBasePos) + BaseOffset);
	FVector DeltaPosition = ConstrainDirectionToPlane(NewWorldPos - UpdatedComponent->GetComponentLocation());

	FHitResult Hit(1.f);
	SafeMoveUpdatedComponent(DeltaPosition, FinalQuat, true, Hit);

	OldBaseLocation = NewBaseLocation;
	OldBaseRotation = NewBaseRotation;*/

	if (!(UpdatedComponent && IsValid(CharacterOwner)))
	{
		return;
	}

	const UPrimitiveComponent* MovementBase = CharacterOwner->GetMovementBase();
	if (!MovementBase)
	{
		return;
	}

	if (!(MovementBase->Mobility == EComponentMobility::Movable))
	{
		return;
	}

	if (!IsValid(MovementBase) || !IsValid(MovementBase->GetOwner()))
	{
		//SetBase(NULL);
		return;
	}

	// Ignore collision with bases during these movements.
	TGuardValue<EMoveComponentFlags> ScopedFlagRestore(MoveComponentFlags, MoveComponentFlags | MOVECOMP_IgnoreBases);

	FQuat DeltaQuat = FQuat::Identity;
	FVector DeltaPosition = FVector::ZeroVector;

	FQuat NewBaseQuat;
	FVector NewBaseLocation;
	if (!GetMovementBaseTransform(MovementBase, FloorResult.FloorHitResult.BoneName, NewBaseLocation, NewBaseQuat))
	{
		return;
	}

	// Find change in rotation
	const bool bRotationChanged = !OldBaseQuat.Equals(NewBaseQuat, 1e-8f);
	if (bRotationChanged)
	{
		DeltaQuat = NewBaseQuat * OldBaseQuat.Inverse();
	}

	// only if base moved
	if (bRotationChanged || (OldBaseLocation != NewBaseLocation))
	{
		// Calculate new transform matrix of base actor (ignoring scale).

		const FQuatRotationTranslationMatrix OldLocalToWorld(OldBaseQuat, OldBaseLocation);
		const FQuatRotationTranslationMatrix NewLocalToWorld(NewBaseQuat, NewBaseLocation);

		if (CharacterOwner->IsMatineeControlled())
		{
			FRotationTranslationMatrix HardRelMatrix(FloorResult.FloorHitResult.Component.Get()->GetComponentRotation(), FloorResult.FloorHitResult.Component.Get()->GetComponentLocation());
			const FMatrix NewWorldTM = HardRelMatrix * NewLocalToWorld;
			const FQuat NewWorldRot = NewWorldTM.ToQuat();
			MoveUpdatedComponent(NewWorldTM.GetOrigin() - UpdatedComponent->GetComponentLocation(), NewWorldRot, true);
		}
		else
		{
			FQuat FinalQuat = UpdatedComponent->GetComponentQuat();

			if (bRotationChanged)
			{
				// Apply change in rotation and pipe through FaceRotation to maintain axis restrictions
				const FQuat PawnOldQuat = UpdatedComponent->GetComponentQuat();
				const FQuat TargetQuat = DeltaQuat * FinalQuat;
				FRotator TargetRotator(TargetQuat);
				CharacterOwner->FaceRotation(TargetRotator, 0.f);
				FinalQuat = UpdatedComponent->GetComponentQuat();

				if (PawnOldQuat.Equals(FinalQuat, 1e-6f))
				{
					// Nothing changed. This means we probably are using another rotation mechanism (bOrientToMovement etc). We should still follow the base object.
					// @todo: This assumes only Yaw is used, currently a valid assumption. This is the only reason FaceRotation() is used above really, aside from being a virtual hook.
					if (CharacterOwner->Controller)///!!!!!!!!!!!!!!!!!!!!!!!!!!
					{
						TargetRotator.Pitch = 0.f;
						TargetRotator.Roll = 0.f;
						MoveUpdatedComponent(FVector::ZeroVector, TargetRotator, false);
						FinalQuat = UpdatedComponent->GetComponentQuat();
					}
				}

				// Pipe through ControlRotation, to affect camera.
				if (CharacterOwner->Controller)
				{
					const FQuat PawnDeltaRotation = FinalQuat * PawnOldQuat.Inverse();
					FRotator FinalRotation = FinalQuat.Rotator();
					UpdateBasedRotation(FinalRotation, PawnDeltaRotation.Rotator());
					FinalQuat = UpdatedComponent->GetComponentQuat();
				}
			}

			// We need to offset the base of the character here, not its origin, so offset by half height
			float HalfHeight, Radius;
			UCapsuleComponent* CapsuleComponent = Cast<UCapsuleComponent>(UpdatedComponent);
			CapsuleComponent->GetScaledCapsuleSize(Radius, HalfHeight);

			FVector const BaseOffset(0.0f, 0.0f, HalfHeight);
			FVector const LocalBasePos = OldLocalToWorld.InverseTransformPosition(UpdatedComponent->GetComponentLocation() - BaseOffset);
			FVector const NewWorldPos = ConstrainLocationToPlane(NewLocalToWorld.TransformPosition(LocalBasePos) + BaseOffset);
			DeltaPosition = ConstrainDirectionToPlane(NewWorldPos - UpdatedComponent->GetComponentLocation());

			// move attached actor
			if (false)//bFastAttachedMove)
			{
				// we're trusting no other obstacle can prevent the move here
				UpdatedComponent->SetWorldLocationAndRotation(NewWorldPos, FinalQuat, false);
			}
			else
			{
				// hack - transforms between local and world space introducing slight error FIXMESTEVE - discuss with engine team: just skip the transforms if no rotation?
				FVector BaseMoveDelta = NewBaseLocation - OldBaseLocation;
				if (!bRotationChanged && (BaseMoveDelta.X == 0.f) && (BaseMoveDelta.Y == 0.f))
				{
					DeltaPosition.X = 0.f;
					DeltaPosition.Y = 0.f;
				}

				FHitResult MoveOnBaseHit(1.f);
				const FVector OldLocation = UpdatedComponent->GetComponentLocation();
				MoveUpdatedComponent(DeltaPosition, FinalQuat, true, &MoveOnBaseHit);
				/*if ((UpdatedComponent->GetComponentLocation() - (OldLocation + DeltaPosition)).IsNearlyZero() == false)
				{
					OnUnableToFollowBaseMove(DeltaPosition, OldLocation, MoveOnBaseHit);
				}*/
			}
		}

		/*if (MovementBase->IsSimulatingPhysics() && CharacterOwner->GetMesh())
		{
			CharacterOwner->GetMesh()->ApplyDeltaToAllPhysicsTransforms(DeltaPosition, DeltaQuat);
		}*/
	}
}

void UPhysBasedMovementComponent::UpdateBasedRotation(FRotator& FinalRotation, const FRotator& ReducedRotation)
{
	AController* Controller = CharacterOwner ? CharacterOwner->Controller : NULL;
	float ControllerRoll = 0.f;
	if (Controller)
	{
		FRotator const ControllerRot = Controller->GetControlRotation();
		ControllerRoll = ControllerRot.Roll;
		Controller->SetControlRotation(ControllerRot + ReducedRotation);
	}

	// Remove roll
	FinalRotation.Roll = 0.f;
	if (Controller)
	{
		FinalRotation.Roll = UpdatedComponent->GetComponentRotation().Roll;
		FRotator NewRotation = Controller->GetControlRotation();
		NewRotation.Roll = ControllerRoll;
		Controller->SetControlRotation(NewRotation);
	}
}

bool UPhysBasedMovementComponent::GetMovementBaseTransform(const UPrimitiveComponent* MovementBase, const FName BoneName, FVector& OutLocation, FQuat& OutQuat)
{
	if (MovementBase)
	{
		if (BoneName != NAME_None)
		{
			bool bFoundBone = false;
			if (MovementBase)
			{
				// Check if this socket or bone exists (DoesSocketExist checks for either, as does requesting the transform).
				if (MovementBase->DoesSocketExist(BoneName))
				{
					MovementBase->GetSocketWorldLocationAndRotation(BoneName, OutLocation, OutQuat);
					bFoundBone = true;
				}
				else
				{
					//UE_LOG(LogCharacter, Warning, TEXT("GetMovementBaseTransform(): Invalid bone or socket '%s' for PrimitiveComponent base %s"), *BoneName.ToString(), *GetPathNameSafe(MovementBase));
				}
			}

			if (!bFoundBone)
			{
				OutLocation = MovementBase->GetComponentLocation();
				OutQuat = MovementBase->GetComponentQuat();
			}
			return bFoundBone;
		}

		// No bone supplied
		OutLocation = MovementBase->GetComponentLocation();
		OutQuat = MovementBase->GetComponentQuat();
		return true;
	}

	// nullptr MovementBase
	OutLocation = FVector::ZeroVector;
	OutQuat = FQuat::Identity;
	return false;
}

bool UPhysBasedMovementComponent::ResolvePenetrationImpl(const FVector& Adjustment, const FHitResult& Hit, const FQuat& NewRotationQuat)
{
	bPositionCorrected |= Super::ResolvePenetrationImpl(Adjustment, Hit, NewRotationQuat);
	return bPositionCorrected;
}

float AngleBetweenVectors(const FVector& A, const FVector& B)
{
	float AngleCosine = FVector::DotProduct(A, B) / (A.Size() * B.Size());
	float AngleRadians = FMath::Acos(AngleCosine);
	return FMath::RadiansToDegrees(AngleRadians);
}

void UPhysBasedMovementComponent::StartClimbing(FVector Newlocation)
{
	UpdatedCapsuleComponent->SetEnableGravity(false);
	UpdatedCapsuleComponent->SetRelativeLocation(Newlocation, true, 0, ETeleportType::ResetPhysics);

	//FVector Down = UpdatedCapsuleComponent->GetUpVector();
	//UpdatedCapsuleComponent->AddImpulse((-Down) * ImpulseForce * UpdatedCapsuleComponent->GetMass());
	UpdatedCapsuleComponent->SetPhysicsLinearVelocity(FVector::ZeroVector);
	SetMovementMode(EPhysBasedMovementMode::CLIMBING);
	IsCanLongJump = false;
}

void UPhysBasedMovementComponent::TickClimbingSystem()
{
	/*///// Angle between two vectors /////*/
	float ForwardVectorLen = 500.0f;
	bool ShowDebug = true;
	float VertTraceForwardOffset = 60.0f;
	float VertTraceUpOffset = 250.0f;
	float VertTraceDownOffset = 300.0f;
	FHitResult HitResult;
	FVector Start, End, UPVector, ForwardOffset;
	bool bOnHit;

	ForwardOffset = UpdatedCapsuleComponent->GetForwardVector() * VertTraceForwardOffset;
	Start = UpdatedCapsuleComponent->GetComponentLocation() + FVector(0.0f, 0.0f, VertTraceUpOffset) + ForwardOffset;
	End = Start - FVector(0.0f, 0.0f, VertTraceDownOffset);

	FVector ForwardVector = UpdatedCapsuleComponent->GetForwardVector();
	FVector StartTrace = UpdatedCapsuleComponent->GetComponentLocation();
	FVector EndTrace = StartTrace + (ForwardVector * ForwardVectorLen);

	FCollisionQueryParams CollisionParams, CollisionParam;


	CollisionParam.AddIgnoredActor(this->GetOwner());
	bOnHit = GetWorld()->LineTraceSingleByChannel(OutHit, Start, End, ECC_Visibility, CollisionParams);

	if (!bOnHit && IsJump)
	{
		IsCanLongJump = true;
	}
	else if (bOnHit)
	{
		GetWorld()->LineTraceSingleByChannel(HitResult, StartTrace, EndTrace, ECC_Visibility, CollisionParam);
		if (ShowDebug)
		{
			//DrawDebugLine(GetWorld(), StartTrace, EndTrace, FColor::Red, false, 1, 0, 1);
		}
		float ForwardWallAngle = FMath::RadiansToDegrees(FMath::Acos(FVector::DotProduct(ForwardVector.GetSafeNormal(), HitResult.ImpactNormal)));
		ForwardWallAngle = 180 - FMath::Abs(ForwardWallAngle);

		CollisionParams.AddIgnoredActor(this->GetOwner());

		//UPVector = UpdatedCapsuleComponent->GetUpVector();

		if (ShowDebug)
		{
			//DrawDebugLine(GetWorld(), Start, End, FColor::Green, false, 1, 0, 1);
		}

		FVector Newlocation;
		Newlocation.X = GetActorLocation().X;
		Newlocation.Y = GetActorLocation().Y;
		Newlocation.Z = OutHit.Location.Z;

		if (bOnHit && IsJump && float(ForwardWallAngle) <= DegreesAngleForStartClimbing)
		{
			StartClimbing(Newlocation);
		}
		if (!bOnHit && !IsJump)
		{
			UpdatedCapsuleComponent->SetEnableGravity(true);
			if (IfOnFloor())
			{
				SetMovementMode(EPhysBasedMovementMode::WALKING);
				IsJump = false;
			}
		}
	}

	IsJump = false;
	/*/////////////////////////////////////*/
}

FVector UPhysBasedMovementComponent::GetInputVector()
{
	return LastForwardInputVector + LastRightInputVector;
}

bool UPhysBasedMovementComponent::IsFalling() const
{
	return !bIsOnFloor;
}
