// Fill out your copyright notice in the Description page of Project Settings.


#include "Character/PhysBasedCharacter.h"

#include "Components/CapsuleComponent.h"
#include "Engine/CollisionProfile.h"
#include "GameFramework/PlayerState.h"


APhysBasedCharacter::APhysBasedCharacter(const FObjectInitializer& objInitializer):Super(objInitializer)
{

	PrimaryActorTick.bCanEverTick = true;

	PhysCharMovementComponent1 = CreateDefaultSubobject<UPhysBasedMovementComponent>(TEXT("PhysCharMoveComp"));

	CapsuleComponent = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Capsule"));
	if (CapsuleComponent)
	{
		CapsuleComponent->InitCapsuleSize(34.0f, 88.0f);
		CapsuleComponent->SetCollisionProfileName(UCollisionProfile::Pawn_ProfileName);

		CapsuleComponent->CanCharacterStepUpOn = ECB_No;
		CapsuleComponent->SetShouldUpdatePhysicsVolume(true);
		CapsuleComponent->SetCanEverAffectNavigation(false);
		CapsuleComponent->bDynamicObstacle = true;

		CapsuleComponent->SetSimulatePhysics(true);
		CapsuleComponent->SetEnableGravity(true);
		CapsuleComponent->SetLinearDamping(0.1);
		CapsuleComponent->SetAngularDamping(0.1);

		//Lock rotation axes for character vertical movement
		if (CapsuleComponent->GetBodyInstance())
		{
			CapsuleComponent->GetBodyInstance()->bLockXRotation = true;
			CapsuleComponent->GetBodyInstance()->bLockYRotation = true;
		}
	}

	RootComponent = CapsuleComponent;
	
}


void APhysBasedCharacter::BeginPlay()
{

	Super::BeginPlay();

}


void APhysBasedCharacter::Tick(float DeltaTime)
{

	Super::Tick(DeltaTime);

}


void APhysBasedCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{

	Super::SetupPlayerInputComponent(PlayerInputComponent);

}


void APhysBasedCharacter::ServerMove_Implementation(FPhysCharMoveInfo MoveInfo)
{
	if (PhysCharMovementComponent1)
	{

		PhysCharMovementComponent1->HandleServerMove(MoveInfo);
	
	}
}


void APhysBasedCharacter::MulticastMove_Implementation(FPhysCharMoveInfo MoveInfo)
{
	if (PhysCharMovementComponent1)
	{
		PhysCharMovementComponent1->HandleMulticastMove(MoveInfo);
	}
}

void APhysBasedCharacter::ClientServerMoveResponse_Implementation(FPhysCharMoveInfo MoveInfo)
{
	if (PhysCharMovementComponent1)
	{
		PhysCharMovementComponent1->HandleServerResponse(MoveInfo);
	}
}



