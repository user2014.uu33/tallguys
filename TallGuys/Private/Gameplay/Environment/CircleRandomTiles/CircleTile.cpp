// Fill out your copyright notice in the Description page of Project Settings.


#include "Gameplay/Environment/CircleRandomTiles/CircleTile.h"

// Sets default values
ACircleTile::ACircleTile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}
// Called when the game starts or when spawned
void ACircleTile::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ACircleTile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ACircleTile::EffectStart_Implementation()
{

}

void ACircleTile::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ACircleTile, ActiveTileEffect);
}