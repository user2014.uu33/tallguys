﻿#include "Gameplay/Environment/CircleRandomTiles/RandCircleFallTiles.h"
#include "Math/UnrealMathUtility.h"
#pragma optimize( "", off )
class ACircleTile;

ARandCircleFallTiles::ARandCircleFallTiles() : Location(0)
{
	PrimaryActorTick.bCanEverTick = true;
	CalcRemoveTiles.Init(0, TileCount);
	Step = 0;
}

void ARandCircleFallTiles::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);
	if (!IsOnConstruct)
	{
		IsOnConstruct = true;
		GenerateSpawn();
	}
}

void ARandCircleFallTiles::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	if (UWorld* World = GetWorld())
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), ACircleTile::StaticClass(), FoundTilesInWorld);
}

void ARandCircleFallTiles::RemovePreConstructTiles()
{
	for (int32 i = 0; i < FoundTilesInWorld.Num(); ++i)
		FoundTilesInWorld[i]->Destroy();
}

void ARandCircleFallTiles::BeginPlay()
{
	Super::BeginPlay();
	srand(time(NULL));

	RemovePreConstructTiles();
	InitArrayForRemoveTiles();		
	GenerateRandomIndexForTile();   // main logic 
	GenerateSpawn();				// arrangement of tiles on the stage
	StartTimerForWaitingPlayers();
}

void ARandCircleFallTiles::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
 
void ARandCircleFallTiles::InitArrayForRemoveTiles()
{
	for (int i = 0; i < TileCount; ++i)
		CalcRemoveTiles[i] = TileCount;
} 

void ARandCircleFallTiles::GenerateSpawn()
{
	if (!TileClass)
	{
		UE_LOG(LogTemp, Warning, TEXT("Could not find Blueprint in TileClass!. Set TileClass to spawn."));
		UE_LOG(LogTemp, Warning, TEXT("Could not find angle in Spawning!. Set value to Rotation."));
		//TODO: Add string to viewport
		return;
	}

	ACircleTile* SpawnBox;
	float Rot = 0.0f;

	float X, Y, Z;
	X = GetActorLocation().X;
	Y = GetActorLocation().Y;
	Z = GetActorLocation().Z;
	Angle = 360 / TileCount; 

	Location = FVector(X, Y, Z);
	if (GetNetMode() == ENetMode::NM_DedicatedServer || GetWorld()->WorldType == EWorldType::Editor)
	{
		for (int i = 0; i < TileCount; ++i)
		{
			Rotation = FRotator(0.0f, Rot, 0.0f);
			SpawnBox = GetWorld()->SpawnActor<ACircleTile>(TileClass, Location, Rotation);
			RemovingTilesDuringTheGame.Add(SpawnBox);
			Rot += Angle;
		}
	}
}

void ARandCircleFallTiles::StartTimerForWaitingPlayers_Implementation()
{
	SetTimer();
}

void ARandCircleFallTiles::RandomFall()
{
	int TileId;
	FTimerHandle TimerHandle;
	if (GetNetMode() == ENetMode::NM_DedicatedServer)
	{
		if (Step == (TileCount - 1))
			return;
		TileId = CalcRemoveTiles[Step];
		UE_LOG(LogTemp, Warning, TEXT("index: %d"), TileId);
		UE_LOG(LogTemp, Warning, TEXT("Delay Before Fall"));
		RemovingTilesDuringTheGame[CalcRemoveTiles[Step]]->EffectStart();
		GetWorldTimerManager().SetTimer(TimerHandle, this, &ARandCircleFallTiles::DelayBeforeFall, 2.0f, false, 5.0f);
	}
}

void ARandCircleFallTiles::DelayBeforeFall()
{
	int TileId;
	if (Step == TileCount - 1)
		return;
	TileId = CalcRemoveTiles[Step];
	RemovingTilesDuringTheGame[TileId]->Destroy();
	Step++;
}

void ARandCircleFallTiles::SetTimer()
{
	if (GetNetMode() == ENetMode::NM_DedicatedServer)
	{
		FTimerHandle TimerHandle;
		GetWorldTimerManager().SetTimer(TimerHandle, this, &ARandCircleFallTiles::RandomFall, NextDelay, true, FirstDelay);
	}
}

void ARandCircleFallTiles::GenerateRandomIndexForTile()
{
	int Iterator = 0;
	int Resrand = 0;

	int Index = 0;
	while (Iterator != TileCount - 1)
	{
		bool Nextstep = true;
		Resrand = rand() % TileCount;
		for (int i = 0; i < TileCount; ++i)
		{
			for (int j = 0; j < TileCount; ++j)
			{
				if (CalcRemoveTiles[j] == Resrand)
				{
					Nextstep = false;
					break;
				}
			}
			if (!Nextstep) 
				break;

			else
			{
				CalcRemoveTiles[Index] = Resrand;
				Iterator++;
				Index++;
				break;
			}
		}
	}
}
