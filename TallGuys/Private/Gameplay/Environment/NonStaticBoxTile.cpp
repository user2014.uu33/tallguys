// Fill out your copyright notice in the Description page of Project Settings.


#include "Gameplay/Environment/NonStaticBoxTile.h"
#include "Components/SceneComponent.h"
#include "Components/BoxComponent.h"

#include "UObject/UObjectGlobals.h"
#include "Components/StaticMeshComponent.h"

#include "Components/ArrowComponent.h"
#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"

ANonStaticBoxTile::ANonStaticBoxTile()
{
	PrimaryActorTick.bCanEverTick = true;
	OverlapComp = CreateDefaultSubobject<UBoxComponent>(TEXT("OverlapComp"));
	OverlapComp->SetBoxExtent(FVector(120, 120, 10));
	RootComponent = OverlapComp;

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	MeshComp->SetupAttachment(RootComponent);

	OverlapComp->OnComponentBeginOverlap.AddDynamic(this, &ANonStaticBoxTile::OverlapTile);
}

void ANonStaticBoxTile::OverlapTile(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	ACharacter* OtherCharacter = Cast<ACharacter>(OtherActor);

	if (OtherCharacter)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ActiveTileEffect, GetActorLocation());
	}
	else if (OtherComp && OtherComp->IsSimulatingPhysics())
	{
		Destroy();
	}
}

