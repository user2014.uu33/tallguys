#include "Gameplay/Environment/labyrinthObstacleManager.h"
#include "Components/SphereComponent.h"

#pragma optimize("", off)

AlabyrinthObstacleManager::AlabyrinthObstacleManager(const FObjectInitializer& ObjectInitializer) : Rot(0, 0, 0)
{
	PrimaryActorTick.bCanEverTick = true;
	DistanceX = 250; DistanceY = 250; DistanceZ = 0;
}

void AlabyrinthObstacleManager::Init()
{
	//AllTiles.Init(0, SizeX * SizeY);
	
	Map = new int* [SizeX];
	for (int i = 0; i < SizeX; ++i)
	{
		Map[i] = new int[SizeY];
	}

}

void AlabyrinthObstacleManager::BeginPlay()
{
	Super::BeginPlay();
	srand(time(NULL));

	Init();
	GenerateLabyrinth();
	SpawnLabyrinth();
}

void AlabyrinthObstacleManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AlabyrinthObstacleManager::SpawnLabyrinth()
{
	if (!TileClass)
	{
		UE_LOG(LogTemp, Warning, TEXT("Could not find Blueprint in TileClass!. Set TileClass to spawn, donkey."));
		//TODO: Add string to viewport
		return;
	}
	float TempX = 0; float TempY = 0, X, Y, Z;
	X = GetActorLocation().X;
	Y = GetActorLocation().Y;
	Z = GetActorLocation().Z;
	if (GetNetMode() == ENetMode::NM_DedicatedServer)
	{
		for (int i = 0; i < SizeX; ++i)
		{
			Map[i][0] = 0;
			Map[i][SizeY - 1] = 0;
		}

		for (int i = 0; i < SizeX; ++i)
		{
			FVector Loc = FVector(TempX, 0, Z);
			for (int j = 0; j < SizeY; ++j)
			{
				TempX = (i * DistanceX) - (float(SizeX - 1) * float(DistanceX)) / 2;
				TempY = (j * DistanceY) - (float(SizeY - 1) * float(DistanceY)) / 2;

				Loc = FVector(TempX, TempY, Z);
				Loc = Loc.RotateAngleAxis(GetActorRotation().Yaw, FVector(0.0, 0.0, 1.0)) + FVector(X, Y, Z);
				
				ABoxTile* SpawnBox = GetWorld()->SpawnActor<ABoxTile>(TileClass, Loc, GetActorRotation(), SpawnParams);
				AllTiles.Add(SpawnBox);
				if (Map[i][j] == 1)
				{
					SpawnBox->SetStatic(true);
				}
				else
				{
					SpawnBox->SetStatic(false);
				}
			}
		}
	}
}

int AlabyrinthObstacleManager::GetRandomNumber(int min, int max)
{
	int num = min + rand() % (max - min + 1);
	return num;
}

void AlabyrinthObstacleManager::GenerateLabyrinth()
{
	int DirX = 0, DirY = 0;
	int Dir = 0, CurrentDir = 0, X = 0, Y = 0;
	CurrentDir = Dir;
	int Iterator = 0;
	if (Iterator == 0)
	{
		X = GetRandomNumber(1, (SizeY - 2));
		Y = 0;
		CurrentDir = Dir;
		Iterator++;
		Map[Y][X] = 1;
		Y = 1;
		Map[Y][X] = 1;
	}

	for (int i = 0; i < SizeX; ++i)
	{
		Map[i][0] = 1;
		Map[i][SizeY - 1] = 1;
	}

	while (t)
	{
		if (Dir == 0)
		{
			DirX = -1;
			DirY = 0;
		}
		else if (Dir == 1)
		{
			DirX = 1;
			DirY = 0;
		}
		else if (Dir == 2)
		{
			DirX = -1;
			DirY = 0;
		}
		else if (Dir == 3)
		{
			DirX = 1;
			DirY = 0;
		}
		else if (Dir == 4)
		{
			DirX = 0;
			DirY = 1;
		}

		if (Map[Y + (DirY - 1)][X + DirX] == 1)
		{
			DirX = 0;
			DirY = 1;
		}

		if (Map[Y + DirY][X + DirX] == 1)
		{
			DirX = -DirX;
			if (Map[Y + DirY][X + DirX] == 1)
			{
				DirX = 0;
				DirY = 1;
			}
		}

		X += DirX;
		Y += DirY;

		if (Y >= SizeY - 1)
		{
			Map[Y][X] = 1;
			t = false;
		}
		else
			Map[Y][X] = 1;

		//change state
		if (CurrentDir == 0 || CurrentDir == 2 || CurrentDir == 3 || CurrentDir == 4)
		{
			Dir = 1;
			CurrentDir = Dir;
		}
		if (CurrentDir == 1)
		{
			Dir = GetRandomNumber(0, 4);
			CurrentDir = Dir;
		}
		if (Y == (SizeX - 1))
			Dir = 4;

		++Iterator;
	}
}

