// Fill out your copyright notice in the Description page of Project Settings.


#include "Gameplay/Environment/GuessingField/GuessingFieldTile.h"
#include "Kismet/GameplayStatics.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
AGuessingFieldTile::AGuessingFieldTile()
{
	OverlapComp = CreateDefaultSubobject<UBoxComponent>(TEXT("OverlapComp"));
	OverlapComp->SetBoxExtent(FVector(400, 400, 25));
	RootComponent = OverlapComp;

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	MeshComp->SetupAttachment(RootComponent);
	OverlapComp->OnComponentBeginOverlap.AddDynamic(this, &AGuessingFieldTile::OverlapTile);
}

void AGuessingFieldTile::OverlapTile(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherComp)
	{
		if (!bIsStatic)
			Destroy();
	}
}

void AGuessingFieldTile::SetStatic(bool flag)
{
	bIsStatic = flag;
}

void AGuessingFieldTile::OnActivate_Implementation()
{

}