#include "Gameplay/Environment/GuessingField/GuessingField.h"

AGuessingField::AGuessingField() : Rot(0)
{
	PrimaryActorTick.bCanEverTick = true;
	SizeX = 4;
	SizeY = 4;
	DistanceX = 900; DistanceY = 900; DistanceZ = 0;
}
void AGuessingField::BeginPlay()
{
	Super::BeginPlay();

	srand(time(NULL));
	Init();
	//GenerateLabyrinth();
	SpawnLabyrinth();
}

void AGuessingField::Init()
{
	Map = new int* [SizeX];
	for (int i = 0; i < SizeX; ++i)
	{
		Map[i] = new int[SizeY];
	}
}

void AGuessingField::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AGuessingField::SpawnLabyrinth()
{
	/*float time = 5.0f;
	float rep = 2.0f;
	GetWorldTimerManager().SetTimer(TimerHandle, this, &AGuessingField::Logic, 10, true, 10);
	TimerHandle.Invalidate();*/

	float TempX = 0; float TempY = 0, X, Y, Z;
	X = GetActorLocation().X;
	Y = GetActorLocation().Y;
	Z = GetActorLocation().Z;
	//if (GetNetMode() == ENetMode::NM_DedicatedServer)
	//{
		for (int i = 0; i < SizeX; ++i)
		{
			Map[i][0] = 0;
			Map[i][SizeY - 1] = 0;
		}

		for (int i = 0; i < SizeX; ++i)
		{
			FVector Loc = FVector(TempX, 0, Z);
			for (int j = 0; j < SizeY; ++j)
			{
				TempX = (i * DistanceX) + X - (float(SizeX) * float(DistanceX)) / 2;
				TempY = (j * DistanceY) + Y - (float(SizeY) * float(DistanceY)) / 2;
				Loc = FVector(TempX, TempY, Z);
				AGuessingFieldTile* SpawnBox = GetWorld()->SpawnActor<AGuessingFieldTile>(TileClass, Loc, Rot, SpawnParams);
				//SpawnBox->SetStatic(true);
				if (Map[i][j] != 1)
				{
					SpawnBox->SetStatic(false);
				}
				else
				{
					SpawnBox->SetStatic(true);
				}
			}
		}
	//}
}


void AGuessingField::Logic()
{
	//TODO Logi�
	//UE_LOG(LogTemp, Warning, TEXT("Time!"));
}

