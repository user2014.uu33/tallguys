#include "Gameplay/Environment/BoxTile.h"
#include "Components/SceneComponent.h"
#include "Components/BoxComponent.h"

#include "UObject/UObjectGlobals.h"
#include "Components/StaticMeshComponent.h"

#include "Components/ArrowComponent.h"
#include "GameFramework/Character.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Kismet/GameplayStatics.h"

class SetMobility;

ABoxTile::ABoxTile()
{
	OverlapComp = CreateDefaultSubobject<UBoxComponent>(TEXT("OverlapComp"));
	OverlapComp->SetBoxExtent(FVector(120, 120, 10));
	RootComponent = OverlapComp;

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	MeshComp->SetupAttachment(RootComponent);
	OverlapComp->OnComponentBeginOverlap.AddDynamic(this, &ABoxTile::OverlapTile);

	bReplicates = true;
}

void ABoxTile::OverlapTile(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
#if 0
	if (OtherComp)
	{
		
	}
#endif
}

void ABoxTile::OnActivate_Implementation()
{

}

void ABoxTile::SetStatic(bool flag)
{
		bIsStatic = flag;
}

