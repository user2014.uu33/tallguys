#include "Gameplay/Game/LevelController.h"

ULevelController::ULevelController(const FObjectInitializer& objInitializer) : Super(objInitializer)
{
	static ConstructorHelpers::FObjectFinder<UDataTable> LevelInfo(TEXT("DataTable'/Game/TallGuys/Data/DT_LevelInfo.DT_LevelInfo'"));
	
	LevelInfo.Object->GetAllRows<FLevelInfo>(contextString, RowNames);
}

void ULevelController::PostInitProperties()
{
	Super::PostInitProperties();
	if (NextLevelRuleClass)
	{
		NextLevelRule = NewObject<UNextLevelRule>(this, *NextLevelRuleClass);
	}
}

void ULevelController::StartNextLevel(int32 PlayerCount)
{
	FString AddName;
	const TArray<FLevelInfo> Levels;
	AddName = NextLevelRule->GetNextLevel(LevelRules, Levels);
	LevelSequence.Add(AddName);

	LevelRules.LevelNum++;
	CurLvl = LevelSequence[LevelRules.LevelNum];

	UE_LOG(LogTemp, Warning, TEXT("Try to start level by URL: %s"), *CurLvl)
	GetWorld()->ServerTravel(CurLvl);
}

void ULevelController::SetNextLvLRuleClass(UNextLevelRule* NextLvLRule)
{

}
