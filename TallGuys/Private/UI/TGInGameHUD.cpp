// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/TGInGameHUD.h"
#include "TimerManager.h"
#include "GameFramework/PlayerState.h"

ATGInGameHUD::ATGInGameHUD()
{
	PrimaryActorTick.bCanEverTick = false;
}

void ATGInGameHUD::BeginPlay()
{
	Super::BeginPlay();
}

void ATGInGameHUD::AddPlayerPassed_Implementation(APlayerState* PlayerState, int PlayerPassedCount, int MaxPlayers)
{

}

void ATGInGameHUD::ShowTimerToStartGame_Implementation()
{
	
}

void ATGInGameHUD::ShowEndGame_Implementation()
{

}

void ATGInGameHUD::ShowRemainingTime_Implementation()
{

}

void ATGInGameHUD::InitUInfo_Implementation(FInGameUInfo Info)
{

}

