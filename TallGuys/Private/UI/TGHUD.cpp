// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/TGHUD.h"


ATGHUD::ATGHUD()
{
}

UUserWidget* ATGHUD::AddWidget(TSubclassOf<class UUserWidget> WidgetClass, bool bShouldCloseCurrentWidget)
{
	APlayerController* PC = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	UUserWidget* Widget = CreateWidget<UUserWidget>(PC, WidgetClass);

	if (bShouldCloseCurrentWidget)
	{
		if(IsValid(CurrentWidget))
			CurrentWidget->RemoveFromViewport();
	}

	if (Widget)
	{ 
		Widget->AddToViewport();
	}

	CurrentWidget = Widget;
	return CurrentWidget;
}

void ATGHUD::RemoveCurrentWidget()
{
	if (CurrentWidget)
	{
		CurrentWidget->RemoveFromParent();
		CurrentWidget->ConditionalBeginDestroy();
	}
}

UUserWidget* ATGHUD::GetCurrentWidget()
{
	if (CurrentWidget)
	{
		return CurrentWidget;
	}
	return nullptr;
}

void ATGHUD::SetBlokingWidget(bool bValue)
{

	/*if (BlokingWidgetClass)
	{
		if (bValue)
		{
			APlayerController* PC = UGameplayStatics::GetPlayerController(GetWorld(), 0);
			if (PC)
			{
				
				if (CurrentBlokingWidget)
				{
					if (!CurrentBlokingWidget->IsInViewport())
					{
						CurrentBlokingWidget->AddToViewport(100);
					}
				}
				else
				{

					CurrentBlokingWidget = CreateWidget<UUserWidget>(PC, BlokingWidgetClass);

					if (CurrentBlokingWidget)
					{
						if (!CurrentBlokingWidget->IsInViewport())
						{
							CurrentBlokingWidget->AddToViewport(100);
						}
					}
					else
					{
						UE_LOG(LogTemp, Warning, TEXT("----- CurrentBlokingWidget is null after spawn"));

					}
				}
			}
		}
		else
		{
			if (CurrentBlokingWidget)
			{
				CurrentBlokingWidget->RemoveFromViewport();
			}
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("----- BlokingWidgetClass is null. Set it in TGHUD."));

	}*/


}