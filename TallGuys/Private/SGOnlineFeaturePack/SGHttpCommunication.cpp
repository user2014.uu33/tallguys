// Fill out your copyright notice in the Description page of Project Settings.

#include "SGOnlineFeaturePack/SGHttpCommunication.h"

#include "HttpModule.h"
#include "Interfaces/IHttpRequest.h"
#include "Interfaces/IHttpResponse.h"


FString USGHttpCommunication::GetVerbAsString(ERequestVerb Verb)
{
	switch (Verb)
	{
	case ERequestVerb::PUT:
		return "PUT";
		break;
	case ERequestVerb::POST:
		return "POST";
		break;
	case ERequestVerb::GET:
		return "GET";
		break;
	case ERequestVerb::DEL:
		return "DELETE";
		break;
	default:
		break;
	}
	return "";
}

EVaRestRequestVerb USGHttpCommunication::GetVerbAsVaRestVerb(ERequestVerb Verb)
{
	switch (Verb)
	{
	case ERequestVerb::PUT:
		return EVaRestRequestVerb::PUT;
		break;
	case ERequestVerb::POST:
		return EVaRestRequestVerb::POST;
		break;
	case ERequestVerb::GET:
		return EVaRestRequestVerb::GET;
		break;
	case ERequestVerb::DEL:
		return EVaRestRequestVerb::DEL;
		break;
	default:
		break;
	}
	return EVaRestRequestVerb::CUSTOM;
}

void USGHttpCommunication::ProcessRequest(FString& Url, ERequestVerb Verb, FString Content)
{
	//make class field?
	/*TSharedRef<IHttpRequest, ESPMode::ThreadSafe> Request = FHttpModule::Get().CreateRequest();

	Request->OnProcessRequestComplete().BindUObject(this, &USGHttpCommunication::OnProcessRequestComplete);

	Request->SetURL(Url);
	Request->SetVerb(GetVerbAsString(Verb));
	 
	Request->SetHeader(TEXT("User-Agent"), TEXT("X-UrealEngine-Agent"));
	Request->SetHeader(TEXT("Content-Type"), TEXT("Application/json"));
	Request->SetContentAsString(Content);

	Request->ProcessRequest();*/

	UVaRestSubsystem* VaRest = GEngine->GetEngineSubsystem<UVaRestSubsystem>();

	UVaRestJsonObject JsonObj;
	//JsonObj.set

	//VaRest->CallURL(Url, GetVerbAsVaRestVerb(Verb), EVaRestRequestContentType::json, &JsonObj, );

}

void USGHttpCommunication::OnProcessRequestComplete(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Blue, *(Response->GetContentAsString()));

	OnHttpResponseDelegate.Broadcast(Request, Response, bWasSuccessful);
}
