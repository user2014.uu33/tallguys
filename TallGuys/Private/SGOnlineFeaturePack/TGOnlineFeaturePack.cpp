// Fill out your copyright notice in the Description page of Project Settings.


#include "SGOnlineFeaturePack/TGOnlineFeaturePack.h"

#include "GenericPlatform/GenericPlatformMisc.h"
#include "Misc/DateTime.h"
#include "Misc/CommandLine.h"

#include "Common/TGCommonStructures.h"


DEFINE_LOG_CATEGORY(TGLogTGOnlineFeaturePack);



void UTGOnlineFeaturePack::OnLoginComplete(int32 LocalUserNumber, bool IsSuccessful, const FUniqueNetId& UserID, const FString& Error)
{
	Super::OnLoginComplete(LocalUserNumber, IsSuccessful, UserID, Error);
	
	UE_LOG(TGLogTGOnlineFeaturePack, Display, TEXT("----- Service auth completed: IsSuccessful: %d"), IsSuccessful);

	if (IsSuccessful)
	{
		UE_LOG(TGLogTGOnlineFeaturePack, Display, TEXT("----- API auth started!!!"));

		IOnlineSubsystem* OnlineSubsystem;
		IOnlineIdentityPtr OnlineSubsystemIdentity;

		OnAuthCompleteDelegate.Clear();
		OnAuthCompleteDelegate.BindUFunction(this, "OnCloudServerAuthComplete");
		
		switch (CurrentLoginSubsystem)
		{
			case ESubsystemType::FACEBOOK:
			{
				OnlineSubsystem = IOnlineSubsystem::Get(FName("facebook"));
				OnlineSubsystemIdentity = OnlineSubsystem->GetIdentityInterface();
					
				FAPIFacebookLoginRequestStamp Stamp;
				FFacebookAuthContent Content;
				Content.deviceId = FGenericPlatformMisc::GetDeviceId();
				Content.accessToken = OnlineSubsystemIdentity->GetUserAccount(UserID)->GetAccessToken();
				Stamp.SetContent(FGoogleAuthContent::StaticStruct(), &Content);
				CallRequest(Stamp, OnAuthCompleteDelegate);
							
				break;
			}
			case ESubsystemType::GOOGLE:
			{
				OnlineSubsystem = IOnlineSubsystem::Get(FName("google"));
				OnlineSubsystemIdentity = OnlineSubsystem->GetIdentityInterface();
			
				FString IdToken;
				OnlineSubsystemIdentity->GetUserAccount(UserID)->GetAuthAttribute(AUTH_ATTR_ID_TOKEN, IdToken);
					
				FAPIGoogleLoginRequestStamp Stamp;
				FGoogleAuthContent Content;
				Content.deviceId = FGenericPlatformMisc::GetDeviceId();
				Content.idToken = IdToken;
				Stamp.SetContent(FGoogleAuthContent::StaticStruct(), &Content);
				CallRequest(Stamp, OnAuthCompleteDelegate);

				break;
			}
			default: 
			{
				break;
			}
		}
	}
}

void UTGOnlineFeaturePack::OnCloudServerAuthComplete(UVaRestRequestJSON* RequestJson)
{
	UE_LOG(TGLogTGOnlineFeaturePack, Display, TEXT("----- Cloud auth completed!!!"));

	if (RequestJson->GetStatus() == EVaRestRequestStatus::Succeeded)
	{
		AccessToken.SetToken(RequestJson->GetResponseObject()->GetStringField("accessToken"));
		RefreshToken.SetToken(RequestJson->GetResponseObject()->GetStringField("refreshToken"));

		SaveTokens();

		UE_LOG(TGLogTGOnlineFeaturePack, Display, TEXT("----- CloudLoginResult: accessToken: %s, refreshToken: %s"), *(AccessToken.GetToken()), *(RefreshToken.GetToken()));
	}
	else
	{
		UE_LOG(TGLogTGOnlineFeaturePack, Warning, TEXT("----- Cloud auth failed!!!"));
	}

	FString ResponseJson = RequestJson->GetResponseObject()->EncodeJsonToSingleString();

	UE_LOG(TGLogTGOnlineFeaturePack, Display, TEXT("----- ResponseJson: %s"), *ResponseJson);

	int32 ResponseCode = RequestJson->GetResponseCode();
	FAuthResult AuthResult;
	AuthResult.bIsSuccess = (ResponseCode < 299)? true : false;
	AuthResult.ResponseCode = ResponseCode;

	AuthCallback.Execute(RequestJson);

	bIsPendingLogin = false;
}


FPlayerAccountInfo UTGOnlineFeaturePack::GetPlayerAccount()
{
	FPlayerAccountInfo PlayerAccountInfo;
	PlayerAccountInfo.AccessToken = AccessToken.GetToken();
	PlayerAccountInfo.RefreshToken = RefreshToken.GetToken();
	TSharedPtr<FJsonObject> JsonObject = AccessToken.GetTokenPayloadJson();
	if (JsonObject.IsValid())
	{
		JsonObject->TryGetStringField("sub", PlayerAccountInfo.Id);
	}

	return PlayerAccountInfo;
}

bool UTGOnlineFeaturePack::GetAllItems(const FVaRestCallDelegate& Callback)
{
	FGetAllItemsRequestStamp Stamp;
	return CallRequestWithTokenValidation(Stamp, Callback);
}

bool UTGOnlineFeaturePack::GetMarketItems(EMarketItemCategory Category, const FVaRestCallDelegate& Callback)
{
	FGetMarketItemsRequestStamp Stamp;
	Stamp.AddQueryParam("category", FString::FromInt(static_cast<int>(Category)));
	return CallRequestWithTokenValidation(Stamp, Callback);
}

bool UTGOnlineFeaturePack::GetUserItems(const FVaRestCallDelegate& Callback)
{
	FGetUserItemsRequestStamp Stamp;
	return CallRequestWithTokenValidation(Stamp, Callback);
}

bool UTGOnlineFeaturePack::UserItemsApply(const TArray<FString> Ids, const FVaRestCallDelegate& Callback)
{
	FApplyUserItemsRequestStamp Stamp;
	FApplyUserItemsContent Content;
	Content.itemsIds = Ids;
	Stamp.SetContent(FApplyUserItemsContent::StaticStruct(), &Content);
	return CallRequestWithTokenValidation(Stamp, Callback);
}

bool UTGOnlineFeaturePack::GetUser(const FVaRestCallDelegate& Callback)
{
	FGetUserInfoRequestStamp Stamp;
	return CallRequestWithTokenValidation(Stamp, Callback);
}

bool UTGOnlineFeaturePack::BuyItem(FString OfferId, const FVaRestCallDelegate& Callback)
{
	FBuyMarketItemRequestStamp Stamp;
	Stamp.AddPathParam("id", OfferId);
	return CallRequestWithTokenValidation(Stamp, Callback);
}

bool UTGOnlineFeaturePack::GetSeasonInfo(const FVaRestCallDelegate& Callback)
{
	FGetSeasonInfoRequestStamp Stamp;
	return CallRequestWithTokenValidation(Stamp, Callback);
}

bool UTGOnlineFeaturePack::SendBattleResult(FPostBattleResultsContent UserInfo, const FVaRestCallDelegate& Callback)
{
	FPostBattleResultsRequestStamp Stamp;
	Stamp.SetContent(FPostBattleResultsContent::StaticStruct(), &UserInfo);
	return CallRequest(Stamp, Callback);
}

bool UTGOnlineFeaturePack::GetBattleResult(FString BattleId, const FVaRestCallDelegate& Callback)
{
	FGetBattleResultStamp Stamp;
	Stamp.AddPathParam("id", BattleId);
	return CallRequestWithTokenValidation(Stamp, Callback);
}

bool UTGOnlineFeaturePack::Enqueue(FString BattleType, ETGServerType ServerType, FString TestBattleId, const FVaRestCallDelegate& Callback)
{
	if (TestBattleId.Len() > 0)
	{
		FEnqueueRequestStamp Stamp;
		FEnqueueTestBattleIdContent Content;
		Content.battleType = BattleType;
		Content.serverType = (uint8)ServerType;
		Content.testBattleId.TrimToNullTerminator();
		Stamp.SetContent(FEnqueueTestBattleIdContent::StaticStruct(), &Content);
		return CallRequestWithTokenValidation(Stamp, Callback);
	}
	else
	{
		FEnqueueRequestStamp Stamp;
		FEnqueueContent Content;
		Content.battleType = BattleType;
		Content.serverType = (uint8)ServerType;
		Stamp.SetContent(FEnqueueContent::StaticStruct(), &Content);
		return CallRequestWithTokenValidation(Stamp, Callback);
	}
	
}

bool UTGOnlineFeaturePack::Dequeue(FString BattleId, const FVaRestCallDelegate& Callback)
{
	FDequeuRequestStamp Stamp;
	Stamp.AddPathParam("battleId", BattleId);
	return CallRequestWithTokenValidation(Stamp, Callback);
}

bool UTGOnlineFeaturePack::AskBattleServer(FString BattleId, const FVaRestCallDelegate& Callback)
{
	FAskBattleRequestStamp Stamp;
	Stamp.AddPathParam("battleId", BattleId);
	return CallRequestWithTokenValidation(Stamp, Callback);
}



bool UTGOnlineFeaturePack::UpdateServerState(FString BattleId, EBattleServerState ServerState, const FVaRestCallDelegate& Callback)
{
	FUpdateBattleServerStateRequestStamp Stamp;
	Stamp.AddPathParam("battleId", BattleId);
	FUpdateBattleServerStateContent Content;
	Content.state = (int32)ServerState;
	Stamp.SetContent(FUpdateBattleServerStateContent::StaticStruct(), &Content);
	return CallRequest(Stamp, Callback);
}

bool UTGOnlineFeaturePack::GetBattleInfoById(FString BattleId, const FVaRestCallDelegate& Callback)
{
	FGetBattleInfoRequestStamp Stamp;
	Stamp.AddPathParam("id", BattleId);
	return CallRequest(Stamp, Callback);
}

bool UTGOnlineFeaturePack::ApplySeasonReward(FString RewardId, const FVaRestCallDelegate& Callback)
{
	FClaimUserSeasonRewardsRequestStamp Stamp;
	Stamp.AddPathParam("id", RewardId);
	return CallRequestWithTokenValidation(Stamp, Callback);
}

bool UTGOnlineFeaturePack::GetConfigVersion(const FVaRestCallDelegate& Callback)
{
	FClientGetConfigRequestStamp Stamp;
	Stamp.AddPathParam("id", "client-app-versions");
	Stamp.AddQueryParam("secret", "8OvnIpLZ5mpee9xJ8D4a9WD9fwFVFt30");
	return CallRequestWithTokenValidation(Stamp, Callback);
}

void UTGOnlineFeaturePack::PrintLogFileToLogTEST()
{
	FString FileContent;
	FFileHelper::LoadFileToString(FileContent, *(FPaths::ProjectLogDir() + "TallGuys.log"));

	UE_LOG(TGLogTGOnlineFeaturePack, Display, TEXT("======================== LogFile read try BEGIN"));

	UE_LOG(TGLogTGOnlineFeaturePack, Display, TEXT("======================== LogFile: %s"), *FileContent);
	
	UE_LOG(TGLogTGOnlineFeaturePack, Display, TEXT("======================== LogFile read try END"));

}



FString UTGOnlineFeaturePack::GetBattleIdFromCommandLine()
{
	FString BattleId;

	if (FParse::Value(FCommandLine::Get(), TEXT("BATTLE_ID"), BattleId))
	{
		BattleId = BattleId.Replace(TEXT("="), TEXT(""));
		UE_LOG(TGLogTGOnlineFeaturePack, Display, TEXT("----- Commant line parsed to get BattleId! BattleId = %s"), *BattleId);

	}
	else
	{
		UE_LOG(TGLogTGOnlineFeaturePack, Warning, TEXT("----- Commant line parse failed on get BattleId"));
	}

	return BattleId;
}

FAPIAuthOutput UTGOnlineFeaturePack::GetAPIAuthInfo(UVaRestRequestJSON* RequestJSON)
{
	FAPIAuthOutput Out;
	FJsonObjectConverter::JsonObjectToUStruct(RequestJSON->GetResponseObject()->GetRootObject(), FAPIAuthOutput::StaticStruct(), &Out);
	return Out;
}

FBattleResultOutput UTGOnlineFeaturePack::GetBattleResultInfo(UVaRestRequestJSON* RequestJSON)
{
	FBattleResultOutput Out;
    FJsonObjectConverter::JsonObjectToUStruct(RequestJSON->GetResponseObject()->GetRootObject(), FBattleResultOutput::StaticStruct(), &Out);
    return Out;
}

FGetAllItemsOutput UTGOnlineFeaturePack::GetAllItemsInfo(UVaRestRequestJSON* RequestJSON)
{
	FGetAllItemsOutput Out;
	FJsonObjectConverter::JsonObjectToUStruct(RequestJSON->GetResponseObject()->GetRootObject(), FGetAllItemsOutput::StaticStruct(), &Out);
	return Out;
}

FGetMarketItemsOutput UTGOnlineFeaturePack::GetMarketItemsInfo(UVaRestRequestJSON* RequestJSON)
{
	FGetMarketItemsOutput Out;
	FJsonObjectConverter::JsonObjectToUStruct(RequestJSON->GetResponseObject()->GetRootObject(), FGetMarketItemsOutput::StaticStruct(), &Out);
	return Out;
}

FGetSeasonInfoOutput UTGOnlineFeaturePack::GetSeasonInfoInfo(UVaRestRequestJSON* RequestJSON)
{
	FGetSeasonInfoOutput Out;
	FJsonObjectConverter::JsonObjectToUStruct(RequestJSON->GetResponseObject()->GetRootObject(), FGetSeasonInfoOutput::StaticStruct(), &Out);
	return Out;
}

FGetUserItemsOutput UTGOnlineFeaturePack::GetUserItemsInfo(UVaRestRequestJSON* RequestJSON)
{
	FGetUserItemsOutput Out;
	FJsonObjectConverter::JsonObjectToUStruct(RequestJSON->GetResponseObject()->GetRootObject(), FGetUserItemsOutput::StaticStruct(), &Out);
	return Out;
}

FApplyUserItemsOutput UTGOnlineFeaturePack::GetApplyedItemsInfo(UVaRestRequestJSON* RequestJSON)
{
	FApplyUserItemsOutput Out;
	FJsonObjectConverter::JsonObjectToUStruct(RequestJSON->GetResponseObject()->GetRootObject(), FApplyUserItemsOutput::StaticStruct(), &Out);
	return Out;
}

FGetUserSeasonRewardOutput UTGOnlineFeaturePack::GetUserSeasonRewardInfo(UVaRestRequestJSON* RequestJSON)
{
	FGetUserSeasonRewardOutput Out;
	FJsonObjectConverter::JsonObjectToUStruct(RequestJSON->GetResponseObject()->GetRootObject(), FGetUserSeasonRewardOutput::StaticStruct(), &Out);
	return Out;
}

FClaimUserSeasonRewardOutput UTGOnlineFeaturePack::GetClaimUserSeasonRewardInfo(UVaRestRequestJSON* RequestJSON)
{
	FClaimUserSeasonRewardOutput Out;
	FJsonObjectConverter::JsonObjectToUStruct(RequestJSON->GetResponseObject()->GetRootObject(), FClaimUserSeasonRewardOutput::StaticStruct(), &Out);
	return Out;
}

FGetUserInfoOutput UTGOnlineFeaturePack::GetUserInfo(UVaRestRequestJSON* RequestJSON)
{
	FGetUserInfoOutput Out;
	FJsonObjectConverter::JsonObjectToUStruct(RequestJSON->GetResponseObject()->GetRootObject(), FGetUserInfoOutput::StaticStruct(), &Out);
	return Out;
}

FEnqueueOutput UTGOnlineFeaturePack::GetEnqueueInfo(UVaRestRequestJSON* RequestJSON)
{
	FEnqueueOutput Out;
	FJsonObjectConverter::JsonObjectToUStruct(RequestJSON->GetResponseObject()->GetRootObject(), FEnqueueOutput::StaticStruct(), &Out);
	return Out;
}

FAskBattleOutput UTGOnlineFeaturePack::GetAskBattleInfo(UVaRestRequestJSON* RequestJSON)
{
	FAskBattleOutput Out;
	FJsonObjectConverter::JsonObjectToUStruct(RequestJSON->GetResponseObject()->GetRootObject(), FAskBattleOutput::StaticStruct(), &Out);
	return Out;
}

FBattleInfoOutput UTGOnlineFeaturePack::GetBattleInfo(UVaRestRequestJSON* RequestJSON)
{
	FBattleInfoOutput Out;
	FJsonObjectConverter::JsonObjectToUStruct(RequestJSON->GetResponseObject()->GetRootObject(), FBattleInfoOutput::StaticStruct(), &Out);
	return Out;
}
