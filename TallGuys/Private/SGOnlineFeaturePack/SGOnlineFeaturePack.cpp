// Fill out your copyright notice in the Description page of Project Settings.


#include "SGOnlineFeaturePack/SGOnlineFeaturePack.h"

#include "Common/TGCommonStructures.h"
#include "Common/Save/TGSaveGame.h"
#include "Kismet/GameplayStatics.h"


DEFINE_LOG_CATEGORY(SGLogSGOnlineFeaturePack);


void USGOnlineFeaturePack::LogResponse(UVaRestRequestJSON* RequestJson)
{
	UE_LOG(SGLogSGOnlineFeaturePack, Display, TEXT("Response to %s"), *(RequestJson->GetURL()));
	UE_LOG(SGLogSGOnlineFeaturePack, Display, TEXT("	Code: %d"), RequestJson->GetResponseCode());
	UE_LOG(SGLogSGOnlineFeaturePack, Display, TEXT("	Content: %s"), *(RequestJson->GetResponseContentAsString()));
}

void USGOnlineFeaturePack::SaveTokens()
{
	
	FPlayerAccountInfo PlayerAccountInfo;
	PlayerAccountInfo.AccessToken = AccessToken.GetToken();
	PlayerAccountInfo.RefreshToken = RefreshToken.GetToken();
	UTGSaveGame* SaveGame = Cast<UTGSaveGame>(UGameplayStatics::CreateSaveGameObject(UTGSaveGame::StaticClass()));
	SaveGame->PlayerAccountInfo = PlayerAccountInfo;
	UGameplayStatics::SaveGameToSlot(SaveGame, TEXT("Auth"), 0);

	UE_LOG(SGLogSGOnlineFeaturePack, Display, TEXT("----- Tokens saved:\n	Acces token: %s\n	Refresh token: %s"), *(PlayerAccountInfo.AccessToken),
		*(PlayerAccountInfo.RefreshToken));
}

void USGOnlineFeaturePack::LoadTokens()
{
	FString ConfAccessToken;
	FString ConfRefreshToken;

	FString LocalSettingIni = FPaths::ProjectDir() + "/Saved/Config/Windows/LocalSettings.ini";

	UE_LOG(SGLogSGOnlineFeaturePack, Display, TEXT("----- Try to load tokens from save"));
	UE_LOG(SGLogSGOnlineFeaturePack, Display, TEXT("-----	Check if save exist: %d"), UGameplayStatics::DoesSaveGameExist(TEXT("Auth"), 0));

	UTGSaveGame* TGSaveGame = Cast<UTGSaveGame>(UGameplayStatics::LoadGameFromSlot(TEXT("Auth"), 0));

	if (TGSaveGame)
	{
		FPlayerAccountInfo PlayerAccountInfo = TGSaveGame->PlayerAccountInfo;
		AccessToken.SetToken(PlayerAccountInfo.AccessToken);
		RefreshToken.SetToken(PlayerAccountInfo.RefreshToken);

		SaveTokens();

		UE_LOG(SGLogSGOnlineFeaturePack, Display, TEXT("-----	Tokens loaded from save: Acces token: %s, Refresh token: %s"),
			*(PlayerAccountInfo.AccessToken),
			*(PlayerAccountInfo.RefreshToken));
	}
	
#if WITH_EDITOR
	
		if (AccessToken.GetToken().IsEmpty())
		{
			UE_LOG(SGLogSGOnlineFeaturePack, Display, TEXT("----- Try to load token from LocalSettingIni config (only for editor)"));
	
			if (GConfig->GetString(TEXT("TGOnlineFeaturePack.CloudServer"), TEXT("AccessToken"), ConfAccessToken, LocalSettingIni) &&
				GConfig->GetString(TEXT("TGOnlineFeaturePack.CloudServer"), TEXT("RefreshToken"), ConfRefreshToken, LocalSettingIni))
			{
				UE_LOG(SGLogSGOnlineFeaturePack, Display, TEXT("----- Tokens loaded from config: Acces token: %s, Refresh token: %s"),
					*(ConfAccessToken),
					*(ConfRefreshToken));

				AccessToken.SetToken(ConfAccessToken);
				RefreshToken.SetToken(ConfRefreshToken);

				SaveTokens();

			}
		}

#endif
}

bool USGOnlineFeaturePack::CallRefreshAccessToken()
{
	bool bIsSuccess = false;
	if(!bIsPendingRefresh)
	{
		if(VaRestSubsystem)
		{
			FTGRefreshAccessTokenStamp Stamp;
			FRefreshAccessTokenContent Content;
			Content.refreshToken = RefreshToken.GetToken();
			Stamp.SetContent(FRefreshAccessTokenContent::StaticStruct(), &Content);

			bIsSuccess = CallRequest(Stamp, OnRefreshTokenCompleteDelegate);

			bIsPendingRefresh = true;
			
			return bIsSuccess;
		}
	}
	else
	{
		bIsSuccess = true;
	}
	
	return bIsSuccess;
}

void USGOnlineFeaturePack::OnRefreshAccessTokenCompleted(UVaRestRequestJSON* RequestJson)
{

	LogResponse(RequestJson);
	UE_LOG(SGLogSGOnlineFeaturePack, Display, TEXT("Refresh completed %d"), RequestJson->GetResponseCode());

	if(RequestJson->GetResponseCode() < 299)
	{
		FRefreshAccessTokenOutput RefreshedTokens;
		FJsonObjectConverter::JsonObjectToUStruct(RequestJson->GetResponseObject()->GetRootObject(), FRefreshAccessTokenOutput::StaticStruct(), &RefreshedTokens);
		AccessToken.SetToken(RefreshedTokens.accessToken);
		RefreshToken.SetToken(RefreshedTokens.refreshToken);
        
		SaveTokens();

        bIsPendingRefresh = false;
        
		UE_LOG(SGLogSGOnlineFeaturePack, Display, TEXT("Request queue (%d requests)"), RequestQueue.Num());
		if(RequestQueue.Num() > 0)
		{
			for(int i = 0; i < RequestQueue.Num(); ++i)
			{
				UE_LOG(SGLogSGOnlineFeaturePack, Display, TEXT("Request queue. Call url %s"), *(RequestQueue[i].Stamp.GetEndPoint()));

				CallRequestWithTokenValidation(RequestQueue[i].Stamp, RequestQueue[i].Delegate);
			}
		} 
	}
	else
	{
		OnMessageAriseDelegate.Broadcast(FSGOnlineFeatureSubsystemMessage{
		ESGOnlineFeatureMessageType::CRITICAL_ERROR,
		ESGOnlineFeatureErrorType::FAILED_TO_REFRESH_TOKEN,
		RequestJson->GetResponseContentAsString()});
	}
	
}

bool USGOnlineFeaturePack::CallRequestAsAuthorizedClient(FSGRequestStamp& RequestInfo,
	const FVaRestCallDelegate& Callback)
{

	RequestInfo.AddHeader("Authorization", "Bearer " + AccessToken.GetToken());
	//UE_LOG(SGLogSGOnlineFeaturePack, Display, TEXT("----- Sending request auth header: %s"), *("Bearer" + AccessToken.GetToken()));


	return CallRequest(RequestInfo, Callback);
	
}

bool USGOnlineFeaturePack::CallRequestWithTokenValidation(FSGRequestStamp& RequestInfo,
                                                          const FVaRestCallDelegate& Callback)
{

	if(!TokenExpirationIsValid(AccessToken))
	{
		if(TokenExpirationIsValid(RefreshToken))
		{
			RequestQueue.Add({RequestInfo, Callback});
			UE_LOG(SGLogSGOnlineFeaturePack, Display, TEXT("Add request ro queue. Added request url: %s"), *(RequestInfo.GetEndPoint()));

			return CallRefreshAccessToken();
		}
		else
		{
			OnMessageAriseDelegate.Broadcast(FSGOnlineFeatureSubsystemMessage{
				ESGOnlineFeatureMessageType::CRITICAL_ERROR,
				ESGOnlineFeatureErrorType::INVALID_REFRESH_TOKEN,
				"TokenExpirationIsValid(RefreshToken) failed"});
			RequestQueue.Empty();
			
			return false;
		}
	}
	else
	{
		CallRequestAsAuthorizedClient(RequestInfo, Callback);
	}


	
	return true;
	
}

void USGOnlineFeaturePack::OnLoginComplete(int32 LocalUserNumber, bool IsSuccessful, const FUniqueNetId& UserID, const FString& Error)
{
	
	UE_LOG(SGLogSGOnlineFeaturePack, Display, TEXT("----- LoginResult: LocalUserNumber: %d, IsSuccessful: %d, UserID: %s, Error: %s"), LocalUserNumber, IsSuccessful, *UserID.ToString(), *Error);
	
	if (IsSuccessful)
	{
		OnSGLoginComplete.Broadcast(LocalUserNumber, IsSuccessful, UserID, Error);
	}
	
}

void USGOnlineFeaturePack::OnLogoutComplete(int32 LocalUserNumber, bool IsSuccessful)
{
	UE_LOG(SGLogSGOnlineFeaturePack, Display, TEXT("----- LogoutResult: IsSuccessful: %d"), IsSuccessful);
}


FString USGOnlineFeaturePack::GetSubsystemName(ESubsystemType Subsystem)
{
	switch (Subsystem)
	{
	case ESubsystemType::FACEBOOK:
		return "facebook";
		break;
	case ESubsystemType::GOOGLE:
		return "google";
		break;
	default:
		
		break;
	}

	return "";
}


void USGOnlineFeaturePack::Initialize(FSubsystemCollectionBase& Collection)
{
	Super::Initialize(Collection);
	
	if (!bWasInitialized)
	{
		UE_LOG(SGLogSGOnlineFeaturePack, Warning, TEXT("===============================Initialize %s"), *(GetName()));

		VaRestSubsystem = GEngine->GetEngineSubsystem<UTGVaRestSubsystem>();

		OnLoginCompleteDelegate = FOnLoginCompleteDelegate::CreateUObject(this, &USGOnlineFeaturePack::OnLoginComplete);
		OnLogoutCompleteDelegate = FOnLogoutCompleteDelegate::CreateUObject(this, &USGOnlineFeaturePack::OnLogoutComplete);
		OnRefreshTokenCompleteDelegate.BindUFunction(this, TEXT("OnRefreshAccessTokenCompleted"));

		LoadTokens();

		bWasInitialized = true;
	}
}

bool USGOnlineFeaturePack::TokenExpirationIsValid(FJWTObject Token)
{

	TSharedPtr<FJsonObject> PayloadJson = Token.GetTokenPayloadJson();

	if (PayloadJson.IsValid())
	{
		//FString DebugJsonContent;
		//TSharedPtr<TJsonWriter<>> Writer = TJsonWriterFactory<>::Create(&DebugJsonContent);
		//FJsonSerializer::Serialize(PayloadJson.ToSharedRef(), *Writer);
		//UE_LOG(SGLogSGOnlineFeaturePack, Display, TEXT("----- Token payload json is: %s!!!"), *DebugJsonContent);


		int64 ExpirationInt;

		if (PayloadJson->TryGetNumberField("exp", ExpirationInt))
		{

			FDateTime Expiration = FDateTime::FromUnixTimestamp(ExpirationInt);

			if (FDateTime::UtcNow() > Expiration)
			{
				return false;
			}

			return true;
		}
		else
		{ 
			UE_LOG(SGLogSGOnlineFeaturePack, Display, TEXT("----- Token expiration is absent!!!"));	
		}

	}
	else
	{
		UE_LOG(SGLogSGOnlineFeaturePack, Display, TEXT("----- Token payload json is invalid!!!"));

	}
		
	PayloadJson.Reset();
	return false;
}

bool USGOnlineFeaturePack::StartLoginWith(ESubsystemType ServiceProvider, const FVaRestCallDelegate& Callback)
{
	bool bIsSuccess = false;

	FString ServiceProviderName = GetSubsystemName(ServiceProvider);

	IOnlineSubsystem* OnlineSubsystem = IOnlineSubsystem::Get(FName(*ServiceProviderName));
	
	if (OnlineSubsystem)
	{
		IOnlineIdentityPtr OnlineSubsystemIdentity = OnlineSubsystem->GetIdentityInterface();

		if (OnlineSubsystemIdentity.IsValid())
		{
			FOnlineAccountCredentials AccountCredentials;
			AccountCredentials.Type = ServiceProviderName;


			OnlineSubsystemIdentity->ClearOnLoginCompleteDelegates(0, this);
			OnlineSubsystemIdentity->AddOnLoginCompleteDelegate_Handle(0, OnLoginCompleteDelegate);

			UE_LOG(SGLogSGOnlineFeaturePack, Display, TEXT("----- Login with %s started"), *ServiceProviderName);

			CurrentLoginSubsystem = ServiceProvider;
			AuthCallback = Callback;
			
			if (!OnlineSubsystemIdentity->Login(0, AccountCredentials))
			{
				UE_LOG(SGLogSGOnlineFeaturePack, Display, TEXT("----- Login %s is failed!!!"), *ServiceProviderName);
			}
		}
		else
		{
			UE_LOG(SGLogSGOnlineFeaturePack, Display, TEXT("----- %s identity is NULL!!!"), *ServiceProviderName);
		}
	}
	else
	{
		UE_LOG(SGLogSGOnlineFeaturePack, Display, TEXT("----- Subsystem %s is NULL!!!"), *ServiceProviderName);
	}

	return bIsSuccess;
}

bool USGOnlineFeaturePack::Logout(ESubsystemType ServiceProvider)
{
	bool bIsSuccess = false;

	FString ServiceProviderName = GetSubsystemName(ServiceProvider);

	IOnlineSubsystem* OnlineSubsystem = IOnlineSubsystem::Get(FName(*ServiceProviderName));

	if (OnlineSubsystem)
	{
		IOnlineIdentityPtr OnlineSubsystemIdentity = OnlineSubsystem->GetIdentityInterface();

		if (OnlineSubsystemIdentity.IsValid())
		{
			OnlineSubsystemIdentity->AddOnLogoutCompleteDelegate_Handle(0, OnLogoutCompleteDelegate);
			UE_LOG(SGLogSGOnlineFeaturePack, Display, TEXT("----- Logout %s started"), *ServiceProviderName);
			OnlineSubsystemIdentity->Logout(0);
		}
	}

	return bIsSuccess;
}

bool USGOnlineFeaturePack::CallRequest(const FSGRequestStamp& RequestInfo, const FVaRestCallDelegate& Callback)
{
	UE_LOG(SGLogSGOnlineFeaturePack, Display, TEXT("----- Sending request URL: %s"), *(RequestInfo.GetEndPoint()));

	TArray<FVaRestHeader> Headers;

	for(int i = 0; i < RequestInfo.Headers.Num(); i++)
	{
		Headers.Add({RequestInfo.Headers[i].Name, RequestInfo.Headers[i].Value});
	}
	UVaRestJsonObject* VaRestJson = VaRestSubsystem->ConstructVaRestJsonObject();
	VaRestJson->SetRootObject(RequestInfo.ContentJson);
	
	if (Headers.Num() > 0)
	{
		VaRestSubsystem->CallURLCustomHeaders(RequestInfo.GetEndPoint(), RequestInfo.Verb, EVaRestRequestContentType::json, VaRestJson, Callback, Headers);
	}
	else
	{
		VaRestSubsystem->CallURL(RequestInfo.GetEndPoint(), RequestInfo.Verb, EVaRestRequestContentType::json, VaRestJson, Callback);
	}
	
	return true;
}

FTGVersion USGOnlineFeaturePack::GetTGVersion()
{
	FTGVersion VersionInfo;
	GConfig->GetString(TEXT("/Script/EngineSettings.GeneralProjectSettings"), TEXT("ProjectVersion"), VersionInfo.ProjectVersion, GGameIni);
	GConfig->GetString(TEXT("/Script/AndroidRuntimeSettings.AndroidRuntimeSettings"), TEXT("StoreVersion"), VersionInfo.BuildVersion, GEngineIni);
	return VersionInfo;
}
