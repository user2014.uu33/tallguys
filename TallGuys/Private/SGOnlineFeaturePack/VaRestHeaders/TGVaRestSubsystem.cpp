// Fill out your copyright notice in the Description page of Project Settings.


#include "SGOnlineFeaturePack/VaRestHeaders/TGVaRestSubsystem.h"

void UTGVaRestSubsystem::CallURLCustomHeaders(const FString& URL, EVaRestRequestVerb Verb, EVaRestRequestContentType ContentType, UVaRestJsonObject* VaRestJson, const FVaRestCallDelegate& Callback, TArray<FVaRestHeader> Headers)
{
	// Check we have valid data json
	if (VaRestJson == nullptr)
	{
		VaRestJson = ConstructVaRestJsonObject();
	}

	UVaRestRequestJSON* Request = ConstructVaRestRequest();

	Request->SetCustomVerb("PATCH");
	
	Request->SetVerb(Verb);
	Request->SetContentType(ContentType);
	Request->SetRequestObject(VaRestJson);

	//Set own headers
	for (TArray<FVaRestHeader>::TConstIterator It(Headers); It; ++It)
	{
		Request->SetHeader(It->HeaderName, It->HeaderValue);
	}

	FVaRestCallResponse Response;
	Response.Request = Request;
	Response.Callback = Callback;

	Response.CompleteDelegateHandle = Request->OnStaticRequestComplete.AddUObject(this, &UVaRestSubsystem::OnCallComplete);
	Response.FailDelegateHandle = Request->OnStaticRequestFail.AddUObject(this, &UVaRestSubsystem::OnCallComplete);

	RequestMap.Add(Request, Response);

	Request->ResetResponseData();
	Request->ProcessURL(URL);
}