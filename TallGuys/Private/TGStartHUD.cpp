// Fill out your copyright notice in the Description page of Project Settings.


#include "TGStartHUD.h"


void ATGStartHUD::ShowConnect_Implementation()
{

}

void ATGStartHUD::ShowLobby_Implementation()
{

}

void ATGStartHUD::ShowWardrobe_Implementation()
{

}

void ATGStartHUD::ShowShop_Implementation()
{

}

void ATGStartHUD::ShowSeasonPass_Implementation()
{

}

void ATGStartHUD::ShowShopPreview_Implementation(const FString& OfferId, const FString& Name)
{

}

void ATGStartHUD::UpdateCoins_Implementation(int coins)
{

}

void ATGStartHUD::UpdateShop_Implementation()
{

}

void ATGStartHUD::ShowOfferInfo_Implementation(const FString& OfferId)
{

}

void ATGStartHUD::ShowAfterBattleInfo_Implementation()
{

}
