// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class TallGuysTarget : TargetRules
{
	public TallGuysTarget( TargetInfo Target) : base(Target)
	{
		BuildEnvironment = TargetBuildEnvironment.Unique;
		bUseLoggingInShipping = true;
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.AddRange( new string[] { "TallGuys" } );
	}
}
